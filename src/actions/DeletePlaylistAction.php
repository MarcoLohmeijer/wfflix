<?php


namespace src\actions;

use src\controllers\VideoController;
use src\enums\Routes;
use src\helpers\Redirect;

class DeletePlaylistAction extends VideoController
{
    public function delete()
    {
        if ($this->deletePlaylist($_POST['playlist_id'])){
            Redirect::to(Routes::MY_PLAYLISTS, 'message=delete-success');
        }

        Redirect::to(Routes::MY_PLAYLISTS, 'message=delete-failed');
    }
}