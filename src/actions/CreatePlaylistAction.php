<?php

namespace src\actions;

use Exception;
use src\controllers\PlaylistController;
use src\enums\Routes;
use src\factories\PlaylistFactory;
use src\helpers\Redirect;
use src\models\Playlist;

class CreatePlaylistAction extends PlaylistController
{
    /** @var Playlist  */
    private $playlist;

    /**
     * CreatePlaylistAction constructor.
     * @throws Exception
     */
    public function __construct()
    {
        parent::__construct();
        $this->checkEmptyFields($_POST['name']);
        $this->playlist = PlaylistFactory::create($_POST['name'], $_POST['user_id']);
    }

    /**
     * Handle create playlist
     * @throws Exception
     */
    public function create()
    {
        if ($playlist = $this->doCreate($this->playlist)) {
            Redirect::to(Routes::MY_PLAYLISTS, 'message=success');
        }

        Redirect::to(Routes::MY_PLAYLISTS, 'error=creating-playlist-went-wrong');
    }

    /**
     * @param $name
     */
    private function checkEmptyFields($name)
    {
        if (empty($name)) {
            Redirect::to(Routes::CREATE_PLAYLIST, 'error=playlist-name-is-empty');
        }
    }
}