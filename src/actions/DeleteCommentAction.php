<?php

namespace src\actions;

use src\controllers\CommentController;
use src\enums\Routes;
use src\helpers\Redirect;

class DeleteCommentAction extends CommentController
{
    /**
     * Handle delete comment
     */
    public function delete()
    {
        if ($this->doDelete($_POST['comment_id'])) {
            Redirect::to(Routes::USER_COMMENTS, 'success=Your comment has been deleted');
        }

        Redirect::to(Routes::USER_COMMENTS, 'error=Deleting went wrong');
    }
}