<?php

namespace src\actions;

use Exception;
use src\controllers\PlaylistController;
use src\controllers\VideoController;
use src\enums\Routes;
use src\helpers\Redirect;

class AddToPlaylistAction extends PlaylistController
{
    private $videoController;
    /**
     * AddToPlaylistAction constructor.
     * @throws Exception
     */
    public function __construct()
    {
        parent::__construct();
        $this->videoController = new VideoController();
        $this->checkIfExists($_POST['vid_id'], $_POST['playlist_id']);
    }

    /**
     * @throws Exception
     */
    public function add()
    {
        if ($this->doAdd($_POST['vid_id'], $_POST['playlist_id'])) {
            Redirect::to(Routes::MY_UPLOADS, 'message=success');
       }

        Redirect::to(Routes::PLAYLIST_OVERVIEW_VIEW, 'error=adding-video-to-playlist-went-wrong&vid_id='. $_POST['vid_id']);
       /**
         * 1. check if playlist exists
         * 2. check if video exists
         */
    }

    /**
     * @param $vidId
     * @param $playlistId
     */
    private function checkIfExists($vidId, $playlistId)
    {
        $video = $this->videoController->getVideo($vidId);

        if (empty($video)) {
            Redirect::to(Routes::PLAYLIST_OVERVIEW_VIEW, 'error=video-not-found&vid_id='. $_POST['vid_id']);
        }

        $playlists= $this->getOne($playlistId);

        if (empty($playlists)) {
            Redirect::to(Routes::PLAYLIST_OVERVIEW_VIEW, 'error=playlist-not-found&vid_id='. $_POST['vid_id']);
        }
    }

}