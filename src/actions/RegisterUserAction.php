<?php

namespace src\actions;

use Exception;
use src\controllers\UserController;
use src\enums\Routes;
use src\factories\UserFactory;
use src\helpers\Hasher;
use src\helpers\Redirect;
use src\models\User;

class RegisterUserAction extends UserController
{
    /** @var User */
    private $user;

    /** @var string */
    private $redirect;

    /**
     * RegisterUserAction constructor.
     * @throws Exception
     */
    public function __construct()
    {
        parent::__construct();
        $this->redirect = $_POST['role'] == 'teacher'? Routes::REGISTER_TEACHER : Routes::REGISTER_STUDENT;
        $this->checkEmptyFields($_POST['username'], $_POST['email'], $_POST['password1'], $_POST['password2']);
        $this->checkPassword($_POST['password1'], $_POST['password2']);
    }

    /**
     * Handle register action
     * @throws Exception
     */
    public function register()
    {
        $hashed = $this->hashPassword($_POST['password1']);
        $this->user = UserFactory::create($_POST['username'], $_POST['email'], $hashed, $_POST['role']);
        $this->checkIfAlreadyExists();

        if ($this->create($this->user)) {
            Redirect::to(Routes::LOGIN, 'message=successful');
        }

        Redirect::to($this->redirect, 'error=register-went-wrong');
    }

    /**
     * @param $password
     * @return false|string
     */
    private function hashPassword($password)
    {
        return Hasher::hash($password);
    }

    /**
     * @param $password
     * @param $password2
     */
    private function checkPassword($password, $password2)
    {
        if ($password!== $password2) {
            Redirect::to($this->redirect, 'error=password-is-not-the-same');
        }
    }

    /**
     * @throws Exception
     */
    private function checkIfAlreadyExists()
    {
        if ($this->getByUsername($this->user->getUsername())) {
            Redirect::to($this->redirect, 'error=username-already-exists');
        }

        if ($this->getByEmail($this->user->getEmail())) {
            Redirect::to($this->redirect, 'error=email-already-exists');
        }
    }

    /**
     * @param $username
     * @param $email
     * @param $password
     * @param $password2
     */
    private function checkEmptyFields($username, $email, $password, $password2)
    {
        if (empty($username)) {
            Redirect::to($this->redirect, 'error=username-is-empty');
        }
        if (empty($email)) {
            Redirect::to($this->redirect, 'error=email-is-empty');
        }
        if (empty($password)) {
            Redirect::to($this->redirect, 'error=password-is-empty');
        }
        if (empty($password2)) {
            Redirect::to($this->redirect, 'error=repeat-password-is-empty');
        }
    }
}