<?php

namespace src\actions;

use Exception;
use src\controllers\VideoController;
use src\enums\Routes;
use src\factories\VideoFactory;
use src\helpers\Redirect;
use src\models\Video;

class EditVideoAction extends VideoController
{
    /** @var Video */
    private $video;

    /**
     * EditVideoAction constructor.
     * @throws Exception
     */
    public function __construct()
    {
        parent::__construct();
        $this->checkEmptyFields($_POST['title'], $_POST['description'], $_POST['video_id']);
        $this->video = VideoFactory::editVideo($_POST['title'], $_POST['description']);
    }

    /**
     * Handle edit video
     */
    public function edit()
    {
        if ($this->doEdit($this->video, $_POST['video_id'])) {
            Redirect::to(Routes::EDIT_VIDEO_VIEW, 'video_id='.$_POST['video_id'].'&success=success');
        }

        Redirect::to(Routes::EDIT_VIDEO_VIEW, 'video_id='.$_POST['video_id'].'&error=update-video-went-wrong');
    }

    /**
     * @param $title
     * @param $description
     */
    public function checkEmptyFields($title, $description, $id)
    {
        if (empty($title)) {
            Redirect::to(Routes::EDIT_VIDEO_VIEW, 'video_id='.$_POST['video_id'].'&error=title-is-empty');
        }

        if (empty($description)) {
            Redirect::to(Routes::EDIT_VIDEO_VIEW, 'video_id='.$_POST['video_id'].'&error=description-is-empty');
        }

        if (empty($id)) {
            Redirect::to(Routes::EDIT_VIDEO_VIEW, 'video_id='.$_POST['video_id'].'&error=id-is-empty');
        }
    }
}