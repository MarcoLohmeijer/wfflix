<?php

namespace src\actions;

use Exception;
use src\controllers\VideoController;
use src\enums\Routes;
use src\helpers\Redirect;

class LikeVideoAction extends VideoController
{
    /**
     * Handles like action
     * @throws Exception
     */
    public function like()
    {
        $videoDetails = $this->getVideo($_POST['vid_id']);

        if ($this->updateLikeDetails($_SESSION['user_id'], $_POST['vid_id'])){
            Redirect::to(Routes::WATCH, 'video='.$_POST['video_url'].'&id='.$_POST['vid_id'].'&message=You-already-liked-this-video!');
        }

        if ($this->updateLikes($_POST['vid_id'], $videoDetails['likes'], $_SESSION['user_id'])) {
            Redirect::to(Routes::WATCH, 'video='.$_POST['video_url'].'&id='.$_POST['vid_id'].'&message=You-liked-this-video!');
        }

        Redirect::to(Routes::WATCH, 'video='.$_POST['video_url'].'&id='.$_POST['vid_id'].'&error=like-went-wrong!');
    }
}