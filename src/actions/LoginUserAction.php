<?php
namespace src\actions;

use Exception;
use src\controllers\UserController;
use src\enums\Routes;
use src\helpers\Redirect;

class LoginUserAction extends UserController
{
    /** @var mixed */
    private $user;

    /**
     * LoginUserAction constructor.
     * @throws Exception
     */
    public function __construct()
    {
        parent::__construct();
        $this->checkEmptyFields($_POST['username'], $_POST['password']);
        $this->user = $this->getByUsername($_POST['username']);
    }

    /**
     * Handle login action
     */
    public function login()
    {
        if (password_verify($_POST['password'], $this->user['password'])) {
            $_SESSION['role'] = $this->user['role'];
            $_SESSION['username'] = $this->user['username'];
            $_SESSION['user_id'] = $this->user['user_id'];
            $_SESSION['email'] = $this->user['email'];
            Redirect::to(Routes::HOME, 'message=successful');
        }

        Redirect::to(Routes::LOGIN, 'error=wrong-credentials');
    }

    /**
     * @param $username
     * @param $password
     */
    private function checkEmptyFields($username, $password)
    {
        if (empty($username)) {
            Redirect::to(Routes::LOGIN, 'error=username-is-empty');
        }

        if (empty($password)) {
            Redirect::to(Routes::LOGIN, 'error=password-is-empty');
        }
    }
}
