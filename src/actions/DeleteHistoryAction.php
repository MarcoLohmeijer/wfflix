<?php

namespace src\actions;

use Exception;
use src\controllers\HistoryController;
use src\enums\Routes;
use src\helpers\Redirect;

class DeleteHistoryAction extends HistoryController
{
    /**
     * Handle delete history
     * @throws Exception
     */
    public function delete()
    {
        if ($this->doDelete($_POST['user_id'])) {
            Redirect::to(Routes::USER_HISTORY, 'success=Your history has been deleted');
        }

        Redirect::to(Routes::USER_HISTORY, 'error=Delete history went wrong');
    }
}