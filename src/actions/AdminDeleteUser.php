<?php

namespace src\actions;

use src\controllers\UserController;
use src\enums\Routes;
use src\helpers\Redirect;

class AdminDeleteUser extends UserController
{
    /**
     * Handle delete user as admin
     */
	public function adminDelete()
	{
		if( $this->deleteById($_POST['id'])){
			Redirect::to(Routes::ADMIN_SHOW_USERS, 'success=User has been deleted');
		}

		Redirect::to(Routes::ADMIN_SHOW_USERS, 'error=delete-failed');
	}
}