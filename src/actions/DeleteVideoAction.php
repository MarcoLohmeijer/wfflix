<?php

namespace src\actions;

use src\controllers\VideoController;
use src\enums\Routes;
use src\helpers\Redirect;

class DeleteVideoAction extends VideoController
{
    public function delete()
    {
        if ($this->doDelete($_POST['video_id'])) {
            Redirect::to(Routes::MY_UPLOADS, 'success=success');
        }

        Redirect::to(Routes::MY_UPLOADS, 'error=deleting-went-wrong');
    }

}