<?php

namespace src\actions;

use Exception;
use src\controllers\UserController;
use src\enums\Routes;
use src\factories\UserFactory;
use src\helpers\Redirect;
use src\models\User;

class UpdateUserAction extends UserController
{
    /** @var User */
    private $user;

    /** @var mixed */
    private $databaseUser;

    /**
     * UpdateUserAction constructor.
     * @throws Exception
     */
    public function __construct()
    {
        parent::__construct();
        $this->checkEmptyFields($_POST['username'], $_POST['email'], $_POST['password']);
        $this->databaseUser = $this->getByUsername($_SESSION['username']);
        $this->user = UserFactory::create($_POST['username'], $_POST['email'], $this->databaseUser['password'], $_POST['role']);
    }

    /**
     * Handle Update user
     * @throws Exception
     */
    public function update()
    {
        if (password_verify($_POST['password'], $this->databaseUser['password'])) {
            if ($user = $this->doUpdate($this->user, $_SESSION['user_id'])) {
                $_SESSION['username'] = $this->user->getUsername();
                $_SESSION['email'] = $this->user->getEmail();

                Redirect::to(Routes::UPDATE_USER, 'success=Your profile has been changed');
            }

            Redirect::to(Routes::UPDATE_USER, 'error=Updating user went wrong');
        }

        Redirect::to(Routes::UPDATE_USER, 'error=Password incorrect');
    }

    /**
     * @param $username
     * @param $email
     * @param $password
     */
    private function checkEmptyFields($username, $email, $password)
    {
        if (empty($username)) {
            Redirect::to(Routes::UPDATE_USER, 'error=username-is-empty');
        }

        if (empty($email)) {
            Redirect::to(Routes::UPDATE_USER, 'error=email-is-empty');
        }

        if (empty($password)) {
            Redirect::to(Routes::UPDATE_USER, 'error=password-is-empty');
        }
    }
}