<?php

namespace src\actions;

use src\controllers\VideoController;
use src\enums\Routes;
use src\helpers\Redirect;

class AdminDeleteVideo extends VideoController
{
	public function adminDelete()
	{
		if ($this->deleteVideo($_POST['id'])){
			Redirect::to(Routes::ADMIN_SHOW_VIDEOS, 'success=Video deleted with succes');
		};
		Redirect::to(Routes::ADMIN_SHOW_VIDEOS, 'error=delete-failed');
	}
}