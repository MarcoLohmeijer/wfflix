<?php

namespace src\actions;

use Exception;
use src\controllers\UserController;
use src\enums\Routes;
use src\factories\UserFactory;
use src\helpers\Hasher;
use src\helpers\Redirect;
use src\models\User;

class UpdatePasswordAction extends UserController
{
    /** @var User */
    private $user;

    /** @var mixed */
    private $databaseUser;

    /**
     * UpdatePasswordAction constructor.
     * @throws Exception
     */
    public function __construct()
    {
        parent::__construct();
        $this->checkEmptyFields($_POST['password'], $_POST['new-password'], $_POST['new-password2']);
        $this->databaseUser = $this->getByUsername($_SESSION['username']);
    }

    /**
     * Handle update password
     * @throws Exception
     */
    public function updatePassword()
    {
        if (password_verify($_POST['password'], $this->databaseUser['password'])) {

            if ($_POST['new-password'] !== $_POST['new-password2']) {
                Redirect::to(Routes::UPDATE_PASSWORD, 'error=new-passwords-are-not-the-same');
            }

            $password = Hasher::hash($_POST['new-password']);
            $this->user = UserFactory::create($this->databaseUser['username'], $this->databaseUser['email'], $password, $_SESSION['role']);

            if ($this->doUpdatepassword($this->user, $_SESSION['user_id'])) {
                Redirect::to(Routes::UPDATE_PASSWORD, 'success=Your password has been changed');
            }

            Redirect::to(Routes::UPDATE_PASSWORD, 'error=update-password-went-wrong');
        }

        Redirect::to(Routes::UPDATE_PASSWORD, 'error=password-is-incorrect');
    }

    /**
     * @param $password
     * @param $newPassword
     * @param $newPassword2
     */
    private function checkEmptyFields($password, $newPassword, $newPassword2)
    {
        if (empty($password)) {
            Redirect::to(Routes::UPDATE_PASSWORD, 'error=password-is-empty');
        }

        if (empty($newPassword)) {
            Redirect::to(Routes::UPDATE_PASSWORD, 'error=new-password-is-empty');
        }

        if (empty($newPassword2)) {
            Redirect::to(Routes::UPDATE_PASSWORD, 'error=new-password2-is-empty');
        }
    }
}