<?php

namespace src\actions;

use Exception;
use src\controllers\CommentController;
use src\enums\Routes;
use src\factories\CommentFactory;
use src\helpers\Redirect;
use src\models\Comment;

class CommentVideoAction extends CommentController
{
    /** @var Comment */
    private $comment;

    /**
     * CommentVideoAction constructor.
     * @throws Exception
     */
    public function __construct()
    {
        parent::__construct();
        $this->comment = CommentFactory::insert($_POST['comment_content'], $_SESSION['user_id'], $_POST['vid_id']);
    }

    /**
     * Handles commenting on video
     * @throws Exception
     */
    public function insertAction(){
        if ($this->insert($this->comment)) {
            Redirect::to(Routes::WATCH, 'video='.$_POST['video'].'&id='.$_POST['vid_id'].'&success=comment-with-success-inserted');
        }

        Redirect::to(Routes::WATCH, 'video='.$_POST['video'].'&id='.$_POST['vid_id'].'&error=commenting-went-wrong!');
    }
}