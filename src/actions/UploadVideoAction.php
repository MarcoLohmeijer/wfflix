<?php

namespace src\actions;

use Exception;
use src\controllers\TagController;
use src\controllers\VideoController;
use src\enums\Routes;
use src\factories\VideoFactory;
use src\helpers\Redirect;
use src\models\Video;

class UploadVideoAction extends VideoController
{
    /** @var Video */
    private $video;

    /** @var TagController  */
    private $tagController;

    /**
     * UploadVideoAction constructor.
     * @throws Exception
     */
    public function __construct()
    {
        parent::__construct();
        $this->tagController = new TagController();
        $this->checkEmptyFields($_POST['title'], $_POST['description'], $_POST['tags_list']);
        $newVideoName = $this->handleVideoUpload();
        $newThumbnailName = $this->handleThumbnailUpload();
        $this->video = VideoFactory::create($_POST['title'], $_POST['description'],$newVideoName,  $_SESSION['user_id'], $newThumbnailName);
    }

    /**
     * Handle uploading action
     */
    public function upload()
    {

        /** set data in database */
        if ($video_id = $this->create($this->video)) {

            /** saves tags in database */
            foreach ($_POST['tags_list'] as $tag) {
                $this->tagController->saveTag($tag , $video_id);
            }
            Redirect::to(Routes::HOME, 'success=Video uploaded with succes');
        }

        Redirect::to(Routes::UPLOAD, 'error=uploading-failed');
    }

    /**
     * Handle video
     * @return string
     */
    private function handleVideoUpload()
    {
//        var_dump($_FILES['video']['error'] === UPLOAD_ERR_OK);
//        die;
        if (isset($_FILES['video']) && $_FILES['video']['error'] === UPLOAD_ERR_OK) {

            /**
             * get details of the uploaded file
             * uploaded video data
             */
            $videoTmpPath = $_FILES['video']['tmp_name'];
            $videoName = $_FILES['video']['name'];
            $videoNameCmps = explode(".", $videoName);
            $videoExtension = strtolower(end($videoNameCmps));

            $newVideoName = md5(time() . $videoName) . '.' . $videoExtension;
            /** check if video extensions are allowed */
            $allowedfileExtensions = array('mp4', 'flv', 'wmv');
            if (in_array($videoExtension, $allowedfileExtensions)) {
                /** directories for where we save the files */
                $uploadVideoDir = 'src/videos/';
                $upload_video_path = $uploadVideoDir . $newVideoName;

                /** set the video in correct directory */
                if (!move_uploaded_file($videoTmpPath, $upload_video_path)) {
                    Redirect::to(Routes::UPLOAD, 'error=Uploading-video-went-wrong');
                }

                return $newVideoName;
            }

            Redirect::to(Routes::UPLOAD, 'error=video-extension-not-supported');
        }

        Redirect::to(Routes::UPLOAD, 'error=videoFile-not-found');
    }

    /**
     * Handle thumbnail
     * @return string
     */
    private function handleThumbnailUpload()
    {
        if (isset($_FILES['thumbnail']) && $_FILES['thumbnail']['error'] === UPLOAD_ERR_OK) {

            /**
             * get details of the uploaded file
             * uploaded thumbnail data
             */
            $thumbnailTmpPath = $_FILES['thumbnail']['tmp_name'];
            $thumbnailName = $_FILES['thumbnail']['name'];
            $thumbnailNameCmps = explode(".", $thumbnailName);
            $thumbnailExtension = strtolower(end($thumbnailNameCmps));

            $newThumbnailName = md5(time() . $thumbnailName) . '.' . $thumbnailExtension;

            /** check if thumbnail extensions are allowed */
            $allowedfileExtensions = array('jpg', 'jpeg', 'png');
            if (in_array($thumbnailExtension, $allowedfileExtensions)) {
                $uploadThumbnailDir = 'src/thumbnails/';
                $upload_thumbnail_path = $uploadThumbnailDir . $newThumbnailName;

                /** set thumbnail in correct directory */
                if (!move_uploaded_file($thumbnailTmpPath, $upload_thumbnail_path)) {
                    Redirect::to(Routes::UPLOAD, 'error=Uploading-thumbnail-went-wrong');
                }

                return $newThumbnailName;
            }

            Redirect::to(Routes::UPLOAD, 'error=thumbnail-extension-not-supported');
        }

        Redirect::to(Routes::UPLOAD, 'error=thumbnailFile-not-found');
    }

    /**
     * @param $title
     * @param $description
     * @param $tagList
     */
    private function checkEmptyFields($title, $description, $tagList)
    {
        if (empty($title)) {
            Redirect::to(Routes::UPLOAD, 'error=title-is-empty');
        }

        if (empty($description)) {
            Redirect::to(Routes::UPLOAD, 'error=description-is-empty');
        }

        if (empty($tagList)) {
            Redirect::to(Routes::UPLOAD, 'error=tagList-is-empty');
        }
    }
}