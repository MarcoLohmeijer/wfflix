<?php


namespace src\actions;

use src\controllers\VideoController;
use src\enums\Routes;
use src\helpers\Redirect;

class DeleteFromPlaylistAction extends VideoController
{
    public function delete()
    {
        if ($this->deleteVideoFromPlaylist($_POST['vid_id'], $_POST['playlist_id'])){
            Redirect::to(Routes::WATCH_PLAYLIST_CONTENT_REDIRECT, 'playlist_id='.$_POST['playlist_id'].'&message=delete-success');
        }

        Redirect::to(Routes::WATCH_PLAYLIST_CONTENT_REDIRECT, 'playlist_id='.$_POST['playlist_id'].'&message=delete-failed');
    }
}