<?php

namespace src\controllers;

use Exception;
use src\models\Playlist;
use src\models\Video;
use src\repositories\PlaylistRepository;

class PlaylistController
{
    /** @var PlaylistRepository  */
    public $playlistRepository;

    /**
     * userController constructor.
     * set repository
     * @throws Exception
     */
    public function __construct()
    {
        $this->playlistRepository = new PlaylistRepository(Database::connect());
    }

    /**
     * @param Playlist $playlist
     *
     * @return Playlist|false
     * @throws Exception
     */
    public function doCreate(Playlist $playlist)
    {
        return $this->playlistRepository->create($playlist)? $playlist: false;
    }

    /**
     * @param $vid_id
     * @param $playlist_id
     *
     * @return bool
     * @throws Exception
     */
    public function doAdd($vid_id, $playlist_id)
    {
        return $this->playlistRepository->add($vid_id, $playlist_id);
    }

    /**
     * @param $playlist_id
     *
     * @return mixed
     * @throws Exception
     */
    public function getOne($playlist_id)
    {
        return $this->playlistRepository->getOne($playlist_id);
    }

	/**
	 * @param $playlist_id
	 * @param $limit
	 *
	 * @return array
	 * @throws Exception
	 */
    public function showNextOfPlaylistLimited($playlist_id, $limit, $currentVideo)
	{
		return $this->playlistRepository->showNextOfPlaylistLimited($playlist_id, $limit, $currentVideo);
	}

	/**
	 * @param $video_id
	 *
	 * @return array
	 * @throws Exception
	 */
	public function showCurrentVideoInPlaylist($video_id)
	{
		return $this->playlistRepository->showCurrentVideoInPlaylist($video_id);
	}
    /**
     * @return string
     */
    public function getCreatePlaylist()
    {
        return 'src/views/createPlaylist.php';
    }

    /**
     * @return string
     */
    public function getPlaylistOverview()
    {
        return 'src/views/playlistOverview.php';
    }

	/**
	 * @return string
	 */
    public function getWatchPlaylist()
	{
		return 'src/views/watchPlaylistVideo.php';
	}
}