<?php


namespace src\controllers;


use Exception;
use src\repositories\TagRepository;

class TagController
{
    /** @var TagRepository */
 	public $tagRepo;

    /**
     * TagController constructor.
     * @throws Exception
     */
	public function __construct()
	{
		$this->tagRepo = new TagRepository(Database::connect());
	}

    /**
     * @return array
     * @throws Exception
     *
     */
	public function showAll()
	{
		return $this->tagRepo->selectAll();
	}

    /**
     * @param $vid_id
     *
     * @return array|string
     * @throws Exception
     */
	public function showTagVideo($vid_id)
	{
		return $this->tagRepo->showTagVideo($vid_id);
	}

    /**
     * @param $tag
     * @param $vid_id
     *
     * @return string
     * @throws Exception
     */
	public function saveTag($tag, $vid_id)
	{
		return $this->tagRepo->saveTag($tag, $vid_id);
	}
}