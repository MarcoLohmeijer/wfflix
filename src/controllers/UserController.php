<?php

namespace src\controllers;

use Exception;
use src\enums\Routes;
use src\helpers\Redirect;
use src\models\User;
use src\repositories\UserRepository;

class UserController
{
    /** @var UserRepository */
    public $userRepo;

    /**
     * userController constructor.
     * set repository
     * @throws Exception
     */
    public function __construct()
    {
        $this->userRepo = new UserRepository(Database::connect());
    }

    /**
     * @param User $user
     *
     * @return User
     * @throws Exception
     */
    public function create(User $user)
    {
        $user_id = $this->userRepo->create($user);
        $this->userRepo->setRole($user_id, $user->getRole());

        return $user;
    }

	/**
	 * @return mixed|string
	 */
    public function showAll()
	{
		return $this->userRepo->getAll();
	}

    /**
     * @param $username
     *
     * @return mixed
     * @throws Exception
     */
    public function getByUsername($username)
    {
        return $this->userRepo->getByUsername($username);
    }

    /**
     * @param $email
     *
     * @return mixed
     * @throws Exception
     */
    public function getByEmail($email)
    {
        return $this->userRepo->getByEmail($email);
    }

    /**
     * @param $user
     * @param $id
     *
     * @return User|false
     * @throws Exception
     */
    public function doUpdate($user, $id)
    {
        return $this->userRepo->update($user, $id)? $user: false;
    }

    /**
     * @param $user
     * @param int $id
     *
     * @return User|false
     * @throws Exception
     */
    public function doUpdatePassword($user, $id)
    {
        return  $this->userRepo->UpdateUserPassword($user, $id)? $user: false;
    }

    /**
     * @param $username
     *
     * @return bool|UserRepository
     * @throws Exception
     */
    public function delete($username)
    {
        return $this->userRepo->deleteByUsername($username);
    }

	public function deleteById($id)
	{
		return $this->userRepo->deleteById($id);
	}

	public function logout()
    {
        session_unset();
        session_destroy();

        Redirect::to(Routes::LOGIN, 'message=successful-logged-out');
    }

    /**
     * @return string
     */
    public function getLogin()
    {
        return 'src/views/login.php';
    }

    /**
     * @return string
     */
    public function getRegister()
    {
        return 'src/views/registerFirstScreen.php';
    }

    /**
     * @return string
     */
    public function getRegisterTeacher()
    {
        return 'src/views/registerTeacher.php';
    }

    /**
     * @return string
     */
    public function getRegisterStudent()
    {
        return 'src/views/registerStudent.php';
    }

    /**
     * @return string
     */
    public function getUpdateUser()
    {
        return 'src/views/updateUser.php';
    }

    /**
     * @return string
     */
    public function getUpdatePassword()
    {
        return 'src/views/newPassword.php';
    }

    /**
     * @return string
     */
    public function getMyUploads()
    {
        return 'src/views/myUploads.php';
    }

    public function getMyPlaylists()
    {
        return 'src/views/myPlaylists.php';
    }

    public function getPlaylistContent()
    {
        return 'src/views/watchPlaylistContent.php';
    }
}