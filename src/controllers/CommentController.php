<?php

namespace src\controllers;

use Exception;
use src\factories\CommentFactory;
use src\models\Comment;
use src\repositories\HistoryRepository;
use src\repositories\CommentRepository;


class CommentController
{

    /** @var CommentRepository */
    private $commentRepository;

    /** @var HistoryRepository */
    private $historyRepository;

    /**
     * VideoController constructor.
     * set repositories
     * @throws Exception
     */
    public function __construct()
    {
        $this->commentRepository = new CommentRepository(Database::connect());
        $this->historyRepository = new HistoryRepository(Database::connect());
    }

    /**
     * @param Comment $comment
     *
     * @return Comment|false
     * @throws Exception
     */
    public function insert(Comment $comment)
    {
        return $this->commentRepository->insert($comment)? $comment: false;
    }

    /**
     * @param $userId
     *
     * @return array|string
     * @throws Exception
     */
    public function showUserComment($userId)
    {
        return $this->commentRepository->showUserComment($userId);
    }

    /**
     * @param $videoId
     *
     * @return array
     * @throws Exception
     */
    public function showComment($videoId)
    {
        return $this->commentRepository->showComment($videoId);
    }

    public function doDelete($comment_id)
    {
        return $this->commentRepository->delete($comment_id);
    }

    /**
     * @return string
     */
    public function  getComments()
    {
        return 'src/views/myComments.php';
    }
}