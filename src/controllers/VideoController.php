<?php

namespace src\controllers;

use Exception;
use src\factories\HistoryFactory;
use src\models\Video;
use src\repositories\HistoryRepository;
use src\repositories\TagRepository;
use src\repositories\VideoRepository;

class VideoController
{
    /** @var VideoRepository */
    private $videoRepository;

    /** @var HistoryRepository */
    private $historyRepository;

    /** @var TagRepository */
    private $tagRepository;

    /**
     * VideoController constructor.
     * set repositories
     * @throws Exception
     */
    public function __construct()
    {
        $this->videoRepository = new VideoRepository(Database::connect());
        $this->historyRepository = new HistoryRepository(Database::connect());
        $this->tagRepository = new TagRepository(Database::connect());
    }

    /**
     * @param Video $video
     *
     * @return false|string
     * @throws Exception
     */
    public function create(Video $video)
    {
        $video_id =  $this->videoRepository->create($video);

        return $video_id? $video_id: false;
    }


    /**
     * @param $id
     *
     * @return mixed|string
     * @throws Exception
     */
    public function getVideo($id)
    {
        return $this->videoRepository->getOne($id);
    }

    /**
     * @return mixed|string
     * @throws Exception
     */
    public function showAll()
    {
        return $this->videoRepository->getAll();
    }

    /**
     * @param $max
     * @return array
     * @throws Exception
     */
    public function showSuggested($max)
    {
        $histories = $this->historyRepository->getById($_SESSION['user_id']);

        if (empty($histories)) {
            return [];
        }

        $tags = [];
        foreach ($histories as $history) {
            $databaseTags = $this->tagRepository->getByHistory($history['video_id']);
            foreach ($databaseTags as $databaseTag) {
                if (array_key_exists($databaseTag['tag'], $tags)) {
                    $data = $tags[$databaseTag['tag']];
                    $data ++;
                    $tags[$databaseTag['tag']] = $data;
                } else {
                    $tags[$databaseTag['tag']] = 1;
                }
            }
        }

        asort($tags);
        $reverseTags = array_reverse($tags);
        $results = array_slice($reverseTags, 0, 3);

        $maxedTags =[];
        foreach ($results as $key => $result) {
            $maxedTags[] = $key;
        }

        return $this->videoRepository->getSuggested($max, $maxedTags);
    }

    /**
     * @param $user_id
     * @return array|string
     * @throws Exception
     */
    public function showUserUploads($user_id)
    {
        return $this->videoRepository->getUserUploads($user_id);
    }

    /**
     * @param $user_id
     * @return mixed
     * @throws Exception
     */
    public function showUserPlaylists($user_id)
    {
        return $this->videoRepository->getUserPlaylists($user_id);
    }

    /**
     * @param $id
     *
     * @return mixed|string
     * @throws Exception
     */
    public function showWatchedSuggestions($id)
    {
        return $this->videoRepository->getWatchedSuggestions($id);
    }

    /**
     * @param $id
     *
     * @return bool
     * @throws Exception
     */
    public function deleteVideo($id)
    {
    	return $this->videoRepository->delete($id);
    }

    /**
     * @param $id
     * @param $views
     *
     * @return bool|string
     * @throws Exception
     */
    public function updateViews($id, $views)
    {
        $views++;
        return $this->videoRepository->updateViews($id, $views);
    }

    /**
     * @param $user_id
     * @param $video_id
     *
     * @return bool|string
     * @throws Exception
     */
    public function setHistory($user_id, $video_id)
    {
        $history = HistoryFactory::create($user_id, $video_id);
        return $this->historyRepository->create($history);
    }

    /**
     * @param $videoId
     * @param $likes
     * @param $userId
     *
     * @return mixed
     * @throws Exception
     */

    public function updateLikes($videoId, $likes, $userId)
    {
        $likes++;
        $this->videoRepository->updateLikeDetails($userId, $videoId);
        return $this->videoRepository->updateLikes($videoId, $likes);
    }

    /**
     * @param $userId
     * @param $videoId
     *
     * @return bool
     * @throws Exception
     */
    public function updateLikedetails($userid, $videoid)
    {
        if ($this->videoRepository->checkLikedetails($userid, $videoid)) {
            return true;
        }
        return false;
    }

    public function showPlaylistContent($playlist_id)
    {
        return $this->videoRepository->getPlaylistContent($playlist_id);
    }

    /**
     * @return string
     */
    public function getWatch()
    {
        return 'src/views/watch.php';
    }

    /**
     * @return string
     */
    public function getUpload()
    {
        return 'src/views/upload.php';
    }

    /**
     * @return string
     */
    public function getUpdate()
    {
        return 'src/views/updateVideo.php';
    }

    /**
     * @param $tag
     * @param $limit
     * @return mixed|string
     * @throws Exception
     */
    public function showByTag($tag, $limit)
    {
        return $this->videoRepository->getByTag($tag, $limit);
    }

    /**
     * @param $id
     * @param $playlist_id
     * @return bool
     * @throws Exception
     */
    public function deleteVideoFromPlaylist($id, $playlist_id)
    {
        return $this->videoRepository->deleteFromPlaylist($id, $playlist_id);
    }

    /**
     * @param $playlist_id
     *
     * @return bool
     * @throws Exception
     */
    public function deletePlaylist($playlist_id)
    {
        return $this->videoRepository->deletePlaylist($playlist_id);
    }

    /**
     * @param Video $video
     * @param $video_id
     *
     * @return bool
     * @throws Exception
     */
    public function doEdit(Video $video, $video_id)
    {
        return $this->videoRepository->editVideo($video, $video_id);
    }

    /**
     * @param $video_id
     *
     * @return bool
     * @throws Exception
     */
    public function doDelete($video_id)
    {
        return $this->videoRepository->delete($video_id);
    }
}