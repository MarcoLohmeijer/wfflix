<?php

namespace src\controllers;

use Exception;
use PDO;
use PDOException;
use src\enums\DatabaseSettings;

class Database
{
    /**
     * @return PDO
     * @throws Exception
     */
    public static function connect()
    {
        try {
            $conn = new PDO("mysql:host=" . DatabaseSettings::SERVERNAME . ";dbname=" . DatabaseSettings::DATABASE, DatabaseSettings::USERNAME, DatabaseSettings::PASSWORD);
            // set the PDO error mode to exception
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $conn;
        } catch (PDOException $e) {
            throw new Exception("Connection failed: " . $e->getMessage());
        }
    }
}