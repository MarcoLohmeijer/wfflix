<?php


namespace src\controllers;


class AdminController
{
	public function  getAdminOverview()
	{
		return 'src/views/admin/adminOverview.php';
	}
	public function  getAdminAddUser()
	{
		return 'src/views/admin/adminAddUser.php';
	}
	public function  getAdminShowUsers()
	{
		return 'src/views/admin/adminShowUsers.php';
	}
	public function  getAdminSHowVideos()
	{
		return 'src/views/admin/adminShowVideos.php';
	}
}