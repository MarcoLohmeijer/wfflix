<?php


namespace src\controllers;

use Exception;
use src\repositories\HistoryRepository;

class HistoryController
{
    /**
     * @var HistoryRepository
     */
    private $historyRepository;

    /**
     * HistoryController constructor.
     * @throws Exception
     */
    public function __construct()
    {
        $this->historyRepository = new HistoryRepository(Database::connect());
    }

    /**
     * @param $user_id
     * @return mixed
     * @throws Exception
     */
    public function showHistory($user_id)
    {
        return $this->historyRepository->showHistory($user_id);
    }

    /**
     * @param $user_id
     * @return mixed
     * @throws Exception
     */
    public function doDelete($user_id)
    {
        return $this->historyRepository->deleteHistory($user_id);
    }

    /**
     * @return string
     */
    public function  getHistory()
    {
        return 'src/views/userHistory.php';
    }
}