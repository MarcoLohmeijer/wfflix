<?php

namespace src\models;

class History
{
    /** @var int */
    public $userId;

    /** @var int */
    public $videoId;

    /** @var string */
    public $date;

    /**
     * @param int $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    /**
     * @param int $videoId
     */
    public function setVideoId($videoId)
    {
        $this->videoId = $videoId;
    }

    /**
     * @param string $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }
}