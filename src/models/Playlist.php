<?php

namespace src\models;

class Playlist
{
    /** @var string */
    private $name;

    /** @var int */
    private $user_id;

    /**
     * @param $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param int $user_id
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->user_id;
    }
}