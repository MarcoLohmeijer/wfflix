<?php

use src\enums\Routes;

?>
<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
    <!-- Brand/logo -->
    <a class="navbar-brand" href="<?php echo Routes::HOME ?>">
        <img src="src/img/logo.png" alt="logo" style="width:40px;">
    </a>

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
        <span class="navbar-toggler-icon"></span>
    </button>

    <!-- Links -->
    <div class="collapse navbar-collapse" id="collapsibleNavbar">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="<?php echo Routes::HOME ?>">Home</a>
            </li>

            <?php
            //show upload video when role is teacher
            if (isset($_SESSION['role'])) {
				if ($_SESSION['role'] === "teacher" || $_SESSION['role'] === "admin") {
					echo "<li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"" . Routes::UPLOAD . "\">Upload</a>
                        </li>";
					echo "<li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"" . Routes::MY_UPLOADS . "\">MyUploads</a>
                        </li>";
					//show admin overview when role is admin
					if ($_SESSION['role'] == 'admin') {
						echo "<li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"" . Routes::ADMIN_OVERVIEW . "\">AdminPanel</a>
                          </li>";
					}
				}

				if (isset($_SESSION['user_id'])) {
					echo "<li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"" . Routes::UPDATE_USER . "\">Profile</a>
                        </li>
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"" . Routes::USER_COMMENTS . "\">MyComments</a>
                        </li>
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"" . Routes::MY_PLAYLISTS . "\">MyPlaylists</a>
                        </li>
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"" . Routes::USER_HISTORY . "\">History</a>
                        </li>
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"" . Routes::LOGOUT . "\">Logout</a>
                        </li>";
				} else {
					echo "<li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"/" . Routes::LOGIN . "\">Login</a>
                        </li>
                        <li class=\"nav-item\">
                            <a class=\"nav-link\" href=\"" . Routes::REGISTER . "\">Register</a>
                        </li>";
				}
			}
            ?>
        </ul>
    </div>
</nav>