<?php

$role = isset($_SESSION['role']) ? $_SESSION['role'] : null;

if  ($role === 'student' || empty($_SESSION['role'])){
    echo '<h1 class=\'display-1 text-center text-danger\'>Access Denied</h1>';
    exit;
};