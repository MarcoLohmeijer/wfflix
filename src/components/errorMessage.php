<?php
if (isset($_GET['error'])) {
    echo "<div class=\"alert alert-danger alert-dismissible fade show mt-5\">
            <strong>Error!</strong> " . $_GET['error'] . "
          </div>";
}

if (isset($_GET['success'])) {
    echo "<div class=\"alert alert-success alert-dismissible fade show mt-5\">
            <strong>Success!</strong> " . $_GET['success'] . "
          </div>";
}
