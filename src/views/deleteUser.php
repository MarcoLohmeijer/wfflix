<!DOCTYPE html>
<html lang="en">

<?php include_once 'src/components/head.php'; ?>
<link rel="stylesheet" href="backgroundstyle.css">

<body>
<?php include_once 'src/components/navbar.php'; ?>
<div class="container">
    <head>
        <style>
            body {
                background: transparent;
            }
        </style>
    </head>
    <div class="text-center">
        <?php include_once 'src/components/banner.php'; ?>
        <div class="card" style="background-color: transparent">
            <div class="card-body">
                <h1>Delete User<br></h1>
                <form method="post" action='/delete-user-action'>

                    <div class="form-group">
                        <label for="Username"><h10>Username:</h10></label>
                        <input name="username" type="text" class="form-control" id="username">
                    </div>

                    <div class="form-group">
                        <label for "Password"><h10>Password:</h10></label>
                        <input name="password" type="password" class="form-control" id="password">
                    </div>

                    <input type="submit" value="Submit"
                           onclick="return confirm('Are you sure you want to delete your account?')"
                           class=" btn btn-primary">
                </form>
            </div>
            <?php include_once "src/components/errorMessage.php"; ?>
        </div>
    </div>
</div>
</body>

</html>