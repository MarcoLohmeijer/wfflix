<?php
use src\controllers\TagController;
use src\controllers\VideoController;
use src\enums\Routes;
include_once 'src/components/checkRole.php';

$videoController = new VideoController();

$video = $videoController->getVideo($_GET['video_id']);
?>
<!DOCTYPE html>
<html lang="en">
<?php include_once 'src/components/head.php'; ?>
<body>
<?php include_once 'src/components/navbar.php'; ?>

<div class="container-fluid" style="margin-top: 30px">
    <head>
        <style>
            body {
                background: transparent;
            }
        </style>
    </head>
    <?php include_once 'src/components/banner.php'; ?>
    <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-8">
            <h1 class="text-center">Video upload</h1>
        </div>
        <div class="col-sm-2"></div>
    </div>
    <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-8">
            <div class="row">
                <div class="col-sm-3"></div>
                <div class="col-sm-6">
                    <form action="<?php echo Routes::EDIT_VIDEO_ACTION ?>" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="title" style="color: white">Title:</label>
                            <input type="text" value="<?php echo $video['title'] ?>" class="form-control" name="title" id="title">
                        </div>

                        <div class="form-group">
                            <label for="comment" style="color: white">Comment:</label>
                            <input type="text" class="form-control" value="<?php echo $video['description']?>" name="description" id="comment">
                        </div>
                        <input type="hidden" value="<?php echo $_GET['video_id']?>" name="video_id">
                        <button type="submit" class="btn btn-primary btn-block">Verzend</button><hr>
                    </form>
                    <?php include_once "src/components/errorMessage.php"; ?>
                    <a class="btn-primary" href="<?php echo Routes::MY_UPLOADS?>">Return</a>
                </div>
                <div class="col-sm-3"></div>
            </div>
        </div>
        <div class="col-sm-2"></div>
    </div>

</div>
</div>
</body>
</html>
