<!DOCTYPE html>
<html lang="en">

<?php

use src\controllers\VideoController;
use src\enums\Routes;

include_once 'src/components/head.php';
$videoController = new VideoController();
?>
<?php include_once 'src/components/head.php'; ?>
<body>
<?php include_once 'src/components/navbar.php'; ?>
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-8">
            <div class="row">
                <?php
                foreach ($videoController->showUserPlaylists($_SESSION['user_id']) as $playlist) {
                    echo "
                        <div class='col-sm-4'>
                            <div class=\"card-deck\">
                                <div class=\"card\" >
                                    <div class=\"card-body\">
                                    <p>" . $playlist['name'] . "</p>
                                        <form action='".Routes::ADD_TO_PLAYLIST_ACTION."' method='post'>
                                            <input type='hidden' name='vid_id' value='".$_GET['vid_id']."'>
                                            <input type='hidden' name='playlist_id' value='".$playlist['id']."'>
                                            <button type='submit' class='btn btn-primary'>Add to this playlist</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            </div>
                            ";
                }
                ?>
            </div>
        </div>
        <div class="col-sm-2"></div>
    </div>
</div>
</body>