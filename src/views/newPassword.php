<!DOCTYPE html>
<html lang="en">
<?php use src\enums\Routes;

include_once 'src/components/head.php'; ?>

<body class=>
<?php include_once 'src/components/navbar.php'; ?>
<div class="container">
    <head>
        <style>
            body {
                background: url();
            }
        </style>
    </head>
    <div class="text-center">
        <?php include_once 'src/components/banner.php'; ?>
        <div class="card" style="background-color: transparent">
            <div class="card-body">
                <h1>Change Password<br></h1>
                <form method="post" action="<?php echo Routes::UPDATE_PASSWORD_ACTION?>">
                    <div class="form-group">
                        <label for="usr"><h10>Old password:</h10></label>
                        <input name="password" type="password" class="form-control" id="new-password"></td>
                    </div>

                    <div class="form-group">
                        <label for="usr"><h10>New password:</h10></label>
                        <input name="new-password" type="password" class="form-control" id="new-password">
                    </div>

                    <div class="form-group">
                        <label for="usr"><h10>New password verify:</h10></label>
                        <input name="new-password2" type="password" class="form-control" id="new-password2">
                    </div>
                        <hr>
                    <a href="<?php echo Routes::UPDATE_USER ?>" class="btn btn-primary float-left" role="button">
                        Back
                    </a>
                    <button type="submit" class="btn btn-primary float-right">Submit</button>
                </form><br><br>
                <?php include_once "src/components/errorMessage.php"; ?>
            </div>
        </div>
    </div>
</div>
</body>

</html>