<!DOCTYPE html>
<html lang="en">

<?php

use src\controllers\VideoController;
use src\enums\Routes;

include_once 'src/components/head.php';
$videoController = new VideoController(); ?>
<body class="bg-transparent">
<?php include_once 'src/components/navbar.php'; ?>
<div class="container-fluid">
    <?php include_once 'src/components/banner.php'; ?>
    <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-8">
            <a href="<?php echo Routes::CREATE_PLAYLIST ?>">
                <button type="submit" class="btn btn-danger">Create playlist</button>
                <br>
            </a><br>
        </div>
        <div class="col-sm-2"></div>

        <div class="col-sm-2"></div>
        <div class="col-sm-8">
            <h1>My Playlists</h1>
            <hr style="background-color: white">
            <div class="row">
                <?php
                foreach ($videoController->showUserPlaylists($_SESSION['user_id']) as $playlist) {
                    echo "
                        <div class='col-sm-4'>
                            <div class=\"card-deck\">
                                <div class=\"card\" >
                                    <div class=\"card-body1\">
                                        <p>" . $playlist['name'] . "</p>
                                            <form action='' method='get'>
                                                <input type='hidden' name='uri' value='" . Routes::WATCH_PLAYLIST_CONTENT . "'>
                                                <input type='hidden' name='playlist_id' value='" . $playlist['id'] . "'>
                                                <button type='submit' class='btn btn-primary float-left'>Watch playlist content</button>
                                            </form>
                                            <form action='" . Routes::DELETE_PLAYLIST . "' method='post'>
                                                <input type='hidden' name='playlist_id' value='" . $playlist['id'] . "'>
                                                <button type='submit' class='btn btn-primary float-right'>Delete playlist</button>
                                            </form>
                                        <br>
                                    </div><br>
                                </div>
                            </div>
                        </div>";
                }
                ?>
            </div>
            <?php include_once "src/components/errorMessage.php"; ?>
        </div>
        <div class="col-sm-2"></div>