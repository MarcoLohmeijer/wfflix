<?php

use src\enums\Routes;

?>
<!DOCTYPE html>
<html lang="en">
<?php include_once 'src/components/head.php'; ?>
<body class=>
<?php include_once 'src/components/navbar.php'; ?>
<div class="container">
    <head>
        <style>
            body {
                background: url();
            }
        </style>
    </head>
    <div class="text-center">
        <?php include_once 'src/components/banner.php'; ?>
        <div class="card">
            <div class="card-body">
                <h1>Change User settings<br></h1>
                <form action="<?php echo Routes::UPDATE_USER_ACTION ?>" method="POST">

                    <div class="form-group">
                        <label for="usr"><h10>User name:</h10></label>
                        <input value="<?php echo $_SESSION['username'] ?>" name="username" type="text"
                               class="form-control"
                               id="usr">
                    </div>

                    <div class="form-group">
                        <label for="email"><h10>E-mail:</h10></label>
                        <input value="<?php echo $_SESSION['email'] ?>" name="email" type="text"
                               class="form-control"
                               id="email">
                    </div>

                    <div class="form-group"><label for="pwd"><h10>Current password:</h10></label>
                        <input name="password" type="password" class="form-control" id="pwd">
                    </div>

                    <input type="hidden" name="role" value="<?php echo $_SESSION['role'] ?>">
                    <a href="<?php echo Routes::UPDATE_PASSWORD ?>" class="btn btn-primary float-left" role="button">
                        Change password
                    </a>
                    <button type="submit" class="btn btn-primary float-right">Submit</button>
                </form><br><br>
                <?php include_once "src/components/errorMessage.php"; ?>
            </div>
        </div>
    </div>
</div>
</body>

</html>