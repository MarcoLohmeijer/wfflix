<?php

use src\controllers\VideoController;
use src\enums\Routes;

include_once 'src/components/checkRole.php';
$videoController = new VideoController();
?>
<!DOCTYPE html>
<html lang="en">

<?php include_once 'src/components/head.php'; ?>
<body class="bg-transparent">
<?php include_once 'src/components/navbar.php'; ?>
<div class="container-fluid">
    <?php include_once 'src/components/banner.php'; ?>
    <div class="row">
        <div class="col-sm-3"></div>
        <div class="col-sm-6">
            <a href="<?php echo Routes::CREATE_PLAYLIST ?>">
                <button type="submit" class="btn btn-danger">Create playlist</button>
            </a>
        </div>
        <hr>
    </div>
    <div class="col-sm-3"></div>
</div>
<div class="row">
    <div class="col-sm-3"></div>
    <div class="col-sm-6" style="background-color: rgba(0, 0, 0, 0.6);">
        <h4>My uploads</h4>
        <hr style="background-color: white">
        <?php include_once 'src/components/errorMessage.php'; ?>
        <div class="row">
            <?php
            foreach ($videoController->showUserUploads($_SESSION['user_id']) as $video) {
                echo "
                    <div class=\"col-sm-6\">
                        <div class=\"col-sm-12\">
                        <a class=\"video-card-link\" href=\"" . Routes::WATCH . "&video=" . $video['video_url'] . "&id=" . $video['id'] . "\">
                            <div class=\"card-deck\">
                                <div class=\"card\" >
                                    <div class=\"card-body1\">
                                    <img class=\"card-img-top\" src=\"src/thumbnails/" . $video['thumbnail'] . "\" alt=\"Card image\" style=\"width:100%\">
                                        <h4 class=\"card-title\">" . $video['title'] . "</h4>
                                        <p class=\"card-text\">" . substr($video['description'], 0, 80) . "</p>
                                        <p class=\"card-text float-right\">Views: " . $video['views'] . "</p>
                                        <p class=\"card-text\">Likes: " . $video['likes'] . "</p>
                                        <small class=\"card-text\">" . $video['created_at'] . "</small>
                                    </div>
                                    <div class='btn-group'>
                                        <form action=\"\" method='get'>
                                            <input type='hidden' value='" . Routes::EDIT_VIDEO . "' name='uri'>
                                            <input type='hidden' value='" . $video['id'] . "' name='video_id'>
                                            <button class='btn btn-primary' type='submit'>Edit</button>
                                        </form>
                                        <form action=\"" . Routes::DELETE_VIDEO_ACTION . "\" method='post'>
                                            <input type='hidden' value='" . $video['id'] . "' name='video_id'>
                                            <button class='btn btn-danger' type='submit'>Delete</button>
                                        </form>
                                    </div>
                                    <hr>
                                </div>
                            </div>
                        </a>

                        </div>
                    </div>";
            }
            ?>
        </div>
    </div>
    <div class="col-sm-3"></div>
</div>