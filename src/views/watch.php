<?php

use src\controllers\CommentController;
use src\controllers\TagController;
use src\controllers\VideoController;
use src\enums\App;
use src\enums\Routes;

if (!isset($_GET['video'])) {
    header("Location: http://localhost/home");
    exit;
}
$commentController = new CommentController();
$videoController = new VideoController();
$tagController = new TagController();

//grab video details
$videoDetails = $videoController->getVideo($_GET['id']);
$videoController->updateViews($_GET['id'], $videoDetails['views']);
$videoController->setHistory($_SESSION['user_id'], $_GET['id']);

//grab tag details
$tags = $tagController->showTagVideo($_GET['id']);


?>
<!DOCTYPE html>
<html lang="en">
<?php include_once 'src/components/head.php'; ?>

<body>
<?php include_once 'src/components/navbar.php'; ?>
<div class="container-fluid">
    <head>
        <style>
            body {
                background: transparent;
            }
        </style>
    </head>
    <div class="row">
        <div class="col-sm-1"></div>
        <div class='col-sm-8'>
            <div class="embed-responsive embed-responsive-16by9">
                <video width="100%" height="100%" controls>
                    <source src=" <?php echo App::VIDEO_URL . $_GET['video'] ?>" type="video/mp4">
                    <source src=" <?php echo App::VIDEO_URL . $_GET['video'] ?>" type="video/flv">
                    <source src=" <?php echo App::VIDEO_URL . $_GET['video'] ?>" type="video/wmv">
                    Your browser does not support the video tag.
                </video>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <h3><?php echo $videoDetails['title'] ?></h3>
                </div>

                <div class="col-sm-12">
                    <p class="float-right"></p>
                    <p class="float-right">
                        Likes: <?php echo $videoDetails['likes'] . " - Views: " . $videoDetails['views'] ?></p>
                    <small>Upload date: <?php echo $videoDetails['created_at'] ?></small>
                </div>
            <div class="col-sm-12" >
            <?php
                foreach ($tags as $tag){
                    echo "<span class='badge badge-info mr-1'>" .$tag['tag'] . "</span>";
                }

            ?>
            </div>
                <div class="col-sm-12">
                    <div class="btn-group btn-group-sm float-right">
                        <form method="post" action='<?php echo Routes::LIKE_VIDEO_ACTION ?>'>
                            <input type="hidden" name="vid_id" value=<?php echo $_GET['id']?> >
                            <input type='hidden' name="video_url" value="<?php echo $_GET['video']?>">
                            <button type="submit" class="btn btn-info" >Like</button>
                        </form>
                        <form method="get" action=''>
                            <input type="hidden" name="uri" value=<?php echo Routes::PLAYLIST_OVERVIEW?> >
                            <input type="hidden" name="vid_id" value=<?php echo $_GET['id']?> >
                            <button type="submit" class="btn btn-primary">Add to playlist</button>
                        </form>

                    </div>
                    <br>
                    <hr>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <p>Uploader: <b><?php echo $videoDetails['username'] ?></b></p>
                    <p><?php echo $videoDetails['description'] ?></p>
                    <hr>
                </div>
            </div>
            <h3>Send Comment</h3>
            <div class="container">
                <form method="post" action='<?php echo Routes::COMMENT_VIDEO ?>'>
                    <div class="form-group">
                        <input type="hidden" name="vid_id" value=<?php echo $_GET['id']?> >
                        <input type="hidden" name="video" value=<?php echo $_GET['video']?> >
<!--                        <//input type="text" name="comment_name" id="comment_name" class="form-control" placeholder="Enter Name" />-->
                    </div>
                    <div class="form-group">
                        <textarea name="comment_content" id="comment_content" class="form-control" placeholder="Enter Comment" rows="5"></textarea>
                    </div>
                    <div class="form-group">
                        <input type="submit" name="submit" id="submit" class="btn btn-info" value="Submit" />
                    </div>
                </form>
                <span id="comment_message"></span>
                <br />
            <div class="col-sm-12"
                <div id="display_comment">
                    <div class="col-sm-12">
                        <h3>Comments</h3><hr>
                    </div>
                    <?php
                    foreach ($commentController->showComment($_GET['id']) as $comment){
                        echo"
                    <div class=\"col-sm-8\">
                        <div class='row' >
                           <div class='col-sm-6'><h3> " . $comment['username'] . "</h3></div>
                           <div class='col-sm-6' ><p class='text-right text-top'>". $comment['created_at'] ."</p></div>
                           <div class='col-sm-12'><p> ".$comment ['comment'] ."</p></div>
                        </div>
                    </div>
                    </br></br>";
                    }
                    ?>
                </div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class='row'>
                <?php
                foreach ($videoController->showWatchedSuggestions($_GET['id']) as $video) {
                    echo "
                    <div class=\"col-sm-12\">
                        <a class=\"video-card-link\" href=\"".Routes::WATCH."&video=" . $video['video_url'] . "&id=" . $video['id'] . "\">
                            <div class=\"card-deck\">
                                <div class=\"card\" >
                                    <div class=\"card-body1\">
                                    <img class=\"card-img-top\" src=\"/src/thumbnails/" . $video['thumbnail'] . "\" alt=\"Card image\" style=\"width:100%\">
                                        <h4 class=\"card-title\">" . $video['title'] . "</h4>
                                        <p class=\"card-text\">" . substr($video['description'], 0, 200) . "</p>
                                        <p class=\"card-text float-right\">Views: " . $video['views'] . "</p>
                                        <p class=\"card-text\">Likes: " . $video['likes'] . "</p>
                                        <small class=\"card-text\">" . $video['created_at'] . "</small>
                                    </div>
                                    <hr><br><br>
                                </div>
                            </div>
                        </a>
                    </div>";
                }
                ?>
            </div>
        </div>
    </div>
</div>
</body>
</html>