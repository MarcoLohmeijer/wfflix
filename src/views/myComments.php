<?php

use src\controllers\CommentController;
use src\controllers\HistoryController;
use src\controllers\VideoController;
use src\enums\Routes;

$commnentController = new CommentController();
?>
<!DOCTYPE html>
<html lang="en">
<?php include_once 'src/components/head.php'; ?>
<body class=>
<?php include_once 'src/components/navbar.php'; ?>
<div class="container-fluid">
    <head>
        <style>
            body {
                background: transparent;
            }
        </style>
    </head>
    <div class="text-center">
        <?php include_once 'src/components/banner.php'; ?>
        <div class="card">
            <div class="row">
                <div class="col-sm-2"></div>
                <div class="col-sm-8">
                    <h1>My comments</h1><br>
                    <div class="row">
                        <div class="col-sm-1">
                            ID
                        </div>
                        <div class="col-sm-9">
                            Comments
                        </div>
                        <div class="col-sm-2">
                            Action
                        </div>
                    </div><hr style="background-color: white">

                    <?php
                    foreach ($commnentController->showUserComment($_SESSION['user_id']) as $comment){
                        echo"
                         <div class='row' >
                             <div class=\"col-sm-1\">
                                " . $comment ['id'] . "
                            </div>
                            <div class=\"col-sm-9\">
                                " . $comment ['comment'] . "
                                <br><br><br><br>
                            </div>
                                <div class=\"col-sm-2\">
                                <form method='post' action ='". Routes::DELETE_COMMENT_ACTION."'>
                                   <input type='hidden' name='comment_id' value='" . $comment['id'] . "'>
                                  <button type ='action' class=\"btn btn-primary\" name=\"delete\">Delete</button>
                                </form>
                            </div>
                         </div>";
                    }
                    ?><?php include_once "src/components/errorMessage.php"; ?>
                </div>
            </div>
        </div>
    </div>
</div>
</body>

</html>