<!DOCTYPE html>
<html lang="en">

<?php

use src\controllers\VideoController;
use src\enums\Routes;

include_once 'src/components/head.php';
$videoController = new VideoController(); ?>
<body class="bg-transparent">
<?php include_once 'src/components/navbar.php'; ?>
<div class="container-fluid">
    <?php include_once 'src/components/banner.php'; ?>
    <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-8">
            <form action="<?php echo Routes::CREATE_PLAYLIST_ACTION ?>" method="post">
                Playlist name<br>
                <input type="text" name="name">
                <input type="hidden" name="user_id" value="<?php echo $_SESSION['user_id']?>"><hr>
                <button class="btn btn-danger" type="submit">Create playlist</button>
                <?php include_once "src/components/errorMessage.php"; ?>
                <hr><hr>
            </form>
        </div>
        <div class="col-sm-2"></div>
    </div>
    <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-8">
            <div class="row">
                <?php
                foreach ($videoController->showUserUploads($_SESSION['user_id']) as $video) {
                    echo "
                <div class=\"col-sm-3\">
                        <a class=\"video-card-link\" href=\"/watch?video=" . $video['video_url'] . "&id=" . $video['id'] . "\">
                            <div class=\"card-deck\">
                                <div class=\"card\" >
                                    <div class=\"card-body1\">
                                    <img class=\"card-img-top\" src=\"/src/thumbnails/" . $video['thumbnail'] . "\" alt=\"Card image\" style=\"width:100%\">
                                        <h4 class=\"card-title\">" . $video['title'] . "</h4>
                                        <p class=\"card-text\">" . substr($video['description'], 0, 80) . "</p>
                                        <p class=\"card-text float-right\">Views: " . $video['views'] . "</p>
                                        <p class=\"card-text\">Likes: " . $video['likes'] . "</p>
                                        <small class=\"card-text\">" . $video['created_at'] . "</small>
                                    </div>
                                </div>
                            </div><hr>
                        </a>
                </div>";
                }
                ?>
        </div>
            <?php include_once "src/components/errorMessage.php"; ?>
        <div class="col-sm-2"></div>
    </div>