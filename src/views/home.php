<?php

use src\controllers\VideoController;
use src\enums\Routes;
use src\enums\Tags;
use src\helpers\Redirect;

if (!isset($_SESSION['user_id']) && !isset($_SESSION['username'])) {
    Redirect::to(Routes::LOGIN, 'message=need-to-login');
}

$videoController = new VideoController();
?>
<!DOCTYPE html>
<html lang="en">
<?php include_once "src/components/head.php"; ?>
<body>
<?php include_once "src/components/navbar.php"; ?>
<div class="container-fluid">
    <head>
        <style>
            body {
                background: url();
            }
        </style>
    </head>
    <?php include_once "src/components/banner.php"; ?>
    <div class="row">
        <div class="col-sm-1"></div>
        <div class="col-sm-9">
            <h4>Recommended video's</h4>
            <hr style="background-color: white">
            <div class='row'>
                <?php
                foreach ($videoController->showSuggested(4) as $video) {
                    echo "
                    <div class=\"col-sm-3\">
                    <div class=\"col-sm-12\">
                        <a class=\"video-card-link\" href=\"" . Routes::WATCH . "&video=" . $video['video_url'] . "&id=" . $video['id'] . "\">
                            <div class=\"card-deck\">
                                <div class=\"card\" >
                                    <div class=\"card-body1\">
                                    <img class=\"card-img-top\" src=\"src/thumbnails/" . $video['thumbnail'] . "\" alt=\"Card image\" style=\"width:100%\">
                                        <h4 class=\"card-title\">" . $video['title'] . "</h4>
                                        <p class=\"card-text\">" . substr($video['description'], 0, 80) . "</p>
                                        <p class=\"card-text float-right\">Views: " . $video['views'] . "</p>
                                        <p class=\"card-text\">Likes: " . $video['likes'] . "</p>
                                        <small class=\"card-text\">" . $video['created_at'] . "</small>
                                    </div>
                                    <hr><br><br>
                                </div>
                            </div>
                        </a>
                        </div>
                    </div>";
                }
                ?>
            </div>
        </div>
        <div class="col-sm-1"></div>
    </div>


    <div class="row">
        <div class="col-sm-1"></div>
        <div class="col-sm-9">
            <h4>PHP</h4>
            <hr style="background-color: white">
            <div class='row'>
                <?php
                foreach ($videoController->showByTag(Tags::PHP, 4) as $video) {
                    echo "
                    <div class=\"col-sm-3\">
                    <div class=\"col-sm-12\">
                        <a class=\"video-card-link\" href=\"" . Routes::WATCH . "&video=" . $video['video_url'] . "&id=" . $video['id'] . "\">
                            <div class=\"card-deck\">
                                <div class=\"card\" >
                                    <div class=\"card-body1\">
                                    <img class=\"card-img-top\" src=\"src/thumbnails/" . $video['thumbnail'] . "\" alt=\"Card image\" style=\"width:100%\">
                                        <h4 class=\"card-title\">" . $video['title'] . "</h4>
                                        <p class=\"card-text\">" . substr($video['description'], 0, 80) . "</p>
                                        <p class=\"card-text float-right\">Views: " . $video['views'] . "</p>
                                        <p class=\"card-text\">Likes: " . $video['likes'] . "</p>
                                        <small class=\"card-text\">" . $video['created_at'] . "</small>
                                    </div>
                                    <hr><br><br>
                                </div>
                            </div>
                        </a>
                        </div>
                    </div>";
                }
                ?>
            </div>
        </div>
        <div class="col-sm-1"></div>
    </div>


    <div class="row">
        <div class="col-sm-1"></div>
        <div class="col-sm-9">
            <h4>JAVA</h4>
            <hr style="background-color: white">
            <div class='row'>
                <?php
                foreach ($videoController->showByTag(Tags::JAVA, 4) as $video) {
                    echo "
                    <div class=\"col-sm-3\">
                    <div class=\"col-sm-12\">
                        <a class=\"video-card-link\" href=\"" . Routes::WATCH . "&video=" . $video['video_url'] . "&id=" . $video['id'] . "\">
                            <div class=\"card-deck\">
                                <div class=\"card\" >
                                    <div class=\"card-body1\">
                                    <img class=\"card-img-top\" src=\"src/thumbnails/" . $video['thumbnail'] . "\" alt=\"Card image\" style=\"width:100%\">
                                        <h4 class=\"card-title\">" . $video['title'] . "</h4>
                                        <p class=\"card-text\">" . substr($video['description'], 0, 80) . "</p>
                                        <p class=\"card-text float-right\">Views: " . $video['views'] . "</p>
                                        <p class=\"card-text\">Likes: " . $video['likes'] . "</p>
                                        <small class=\"card-text\">" . $video['created_at'] . "</small>
                                    </div>
                                    <hr><br><br>
                                </div>
                            </div>
                        </a>
                        </div>
                    </div>";
                }
                ?>
            </div>
        </div>
    </div>
    <div class="col-sm-1"></div>

    <div class="row">
        <div class="col-sm-1"></div>
        <div class="col-sm-9">
            <h4>C#</h4>
            <hr style="background-color: white">
            <div class='row'>
                <?php
                foreach ($videoController->showByTag(Tags::Csharp, 4) as $video) {
                    echo "
                    <div class=\"col-sm-3\">
                    <div class=\"col-sm-12\">
                        <a class=\"video-card-link\" href=\"" . Routes::WATCH . "&video=" . $video['video_url'] . "&id=" . $video['id'] . "\">
                            <div class=\"card-deck\">
                                <div class=\"card\" >
                                    <div class=\"card-body1\">
                                    <img class=\"card-img-top\" src=\"src/thumbnails/" . $video['thumbnail'] . "\" alt=\"Card image\" style=\"width:100%\">
                                        <h4 class=\"card-title\">" . $video['title'] . "</h4>
                                        <p class=\"card-text\">" . substr($video['description'], 0, 80) . "</p>
                                        <p class=\"card-text float-right\">Views: " . $video['views'] . "</p>
                                        <p class=\"card-text\">Likes: " . $video['likes'] . "</p>
                                        <small class=\"card-text\">" . $video['created_at'] . "</small>
                                    </div>
                                    <hr><br><br>
                                </div>
                            </div>
                        </a>
                        </div>
                    </div>";
                }
                ?>
            </div>
        </div>
        <div class="col-sm-1"></div>
    </div>


    <div class="row">
        <div class="col-sm-1"></div>
        <div class="col-sm-9">
            <h4>CSS</h4>
            <hr style="background-color: white">
            <div class='row'>
                <?php
                foreach ($videoController->showByTag(Tags::CSS, 4) as $video) {
                    echo "
                    <div class=\"col-sm-3\">
                    <div class=\"col-sm-12\">
                        <a class=\"video-card-link\" href=\"" . Routes::WATCH . "&video=" . $video['video_url'] . "&id=" . $video['id'] . "\">
                            <div class=\"card-deck\">
                                <div class=\"card\" >
                                    <div class=\"card-body1\">
                                    <img class=\"card-img-top\" src=\"src/thumbnails/" . $video['thumbnail'] . "\" alt=\"Card image\" style=\"width:100%\">
                                        <h4 class=\"card-title\">" . $video['title'] . "</h4>
                                        <p class=\"card-text\">" . substr($video['description'], 0, 80) . "</p>
                                        <p class=\"card-text float-right\">Views: " . $video['views'] . "</p>
                                        <p class=\"card-text\">Likes: " . $video['likes'] . "</p>
                                        <small class=\"card-text\">" . $video['created_at'] . "</small>
                                    </div>
                                    <hr><br><br>
                                </div>
                            </div>
                        </a>
                        </div>
                    </div>";
                }
                ?>
            </div>
        </div>
        <div class="col-sm-1"></div>
    </div>

    <div class="row">
        <div class="col-sm-1"></div>
        <div class="col-sm-9">
            <h4>Video's</h4>
            <hr style="background-color: white">
            <div class='row'>
                <?php
                foreach ($videoController->showAll() as $video) {
                    echo "
                    <div class=\"col-sm-3\">
                    <div class=\"col-sm-12\">
                        <a class=\"video-card-link\" href=\"" . Routes::WATCH . "&video=" . $video['video_url'] . "&id=" . $video['id'] . "\">
                            <div class=\"card-deck\">
                                <div class=\"card\" >
                                    <div class=\"card-body1\">
                                    <img class=\"card-img-top\" src=\"src/thumbnails/" . $video['thumbnail'] . "\" alt=\"Card image\" style=\"width:100%\">
                                        <h4 class=\"card-title\">" . $video['title'] . "</h4>
                                        <p class=\"card-text\">" . substr($video['description'], 0, 80) . "</p>
                                        <p class=\"card-text float-right\">Views: " . $video['views'] . "</p>
                                        <p class=\"card-text\">Likes: " . $video['likes'] . "</p>
                                        <small class=\"card-text\">" . $video['created_at'] . "</small>
                                    </div>
                                    <hr><br><br>
                                </div>
                            </div>
                        </a>
                        </div>
                    </div>";
                }
                ?>
            </div>
        </div>
        <div class="col-sm-1"></div>
    </div>

</div>
</body>
</html>