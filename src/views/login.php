<!DOCTYPE html>
<html lang="en">
<head>
<?php use src\enums\Routes;

include_once 'src/components/head.php'; ?>
<body>
<?php include_once 'src/components/navbar.php'; ?>
<div class="container">
    <head>
        <style>
            body {
                background: url();
            }
        </style>
    </head>
    <div class="text-center">
        <?php include_once 'src/components/banner.php'; ?>
        <div class="card">
            <div class="card-body">
                <h1>Login</h1>
                <form method="post" action="<?php echo Routes::LOGIN_ACTION ?>">

                    <div class="form-group">
                        <label for="Username"><h10>Username:</h10></label>
                        <input name="username" type="text" class="form-control" id="Username">
                    </div>

                    <div class="form-group">
                        <label for "Password"><h10>Password:</h10></label>
                        <input name="password" type="password" class="form-control" id="Password">
                    </div>

                    <input type="submit" value="Log in">
                    <a href="<?php echo Routes::REGISTER ?>"><input type="button" value="Register"></a>
                </form>
                <?php include_once "src/components/errorMessage.php"; ?>
            </div>
        </div>
    </div>
</div>
</head>
</body>
</html>