<!DOCTYPE html>
<html lang="en">

<?php

use src\controllers\VideoController;
use src\enums\Routes;

include_once 'src/components/head.php'; ?>
<?php include_once 'src/components/head.php'; ?>
<body>
<?php include_once 'src/components/navbar.php'; ?>
<?php include_once 'src/components/CheckIfAdmin.php' ?>
<div class="container-fluid">
    <head>
        <style>
            body {
                background: transparent;
            }
        </style>
    </head>
    <?php include_once 'src/components/banner.php'; ?>
	<div class="row">
		<div class="col-sm-2"></div>
		<div class="col-sm-8">
            <h1 class="text-center">Admin Panel</h1><hr style="background-color: white">
			<div class="row">
                    <div class="col-sm-4">
                        <div class="card m-5">
                            <div class="card-body1 card-hover">
                                <a href="<?php echo Routes::ADMIN_ADD_USER ?>" class="stretched-link">
                                <h5 class="card-title ">ADD USER</h5>
                                <p class="card-text">Add user and give a role</p>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="card m-5">
                            <a href="<?php echo Routes::ADMIN_SHOW_USERS?>" class="stretched-link">
                            <div class="card-body1 card-hover">
                                <h5 class="card-title ">EDIT USERS</h5>
                                <p class="card-text">See users and delete users</p>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="card m-5">
                            <div class="card-body1 card-hover">
                                <a href="<?php echo Routes::ADMIN_SHOW_VIDEOS ?>" class="stretched-link">
                                <h5 class="card-title ">EDIT VIDEO'S</h5>
                                <p class="card-text">See video details en delete video's</p>
                                </a>
                            </div>
                        </div>
                    </div>
			</div>
		</div>
		<div class="col-sm-2"></div>
	</div>
</div>
</body>