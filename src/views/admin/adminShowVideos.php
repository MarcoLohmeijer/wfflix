<!DOCTYPE html>
<html lang="en">

<?php

use src\controllers\VideoController;
use src\controllers\UserController;
use src\enums\Routes;

include_once 'src/components/head.php'; ?>
<?php include_once 'src/components/head.php'; ?>
<body>
<?php include_once 'src/components/navbar.php'; ?>
<?php include_once 'src/components/CheckIfAdmin.php' ?>
<div class="container-fluid">
    <head>
        <style>
            body {
                background: transparent;
            }
        </style>
    </head>
    <?php include_once 'src/components/banner.php'; ?>
    <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-8">
            <h1 class="text-center">Delete Video</h1><hr>
            <div class="row">
                <div class="col-sm-2">
                    ID
                </div>
                <div class="col-sm-4">
                    Video Name
                </div>
                <div class="col-sm-3">
                    Upload Date
                </div>
                <div class="col-sm-3">
                    Delete
                </div>
                <div class="col-sm-12">
                    <hr style="background-color: white">
                </div>
            </div>
            <div class="row">
				<?php
				$videoController = new VideoController();
				$videos = $videoController->showAll();
				foreach ($videos as $video) {
					echo "
                 <div class=\"col-sm-2\">
                    " . $video ['id'] . "
                </div>
                <div class=\"col-sm-4\">
                    " . $video ['title'] . "
                </div>
                <div class='col-sm-3'> 
                    " .$video['created_at']. "
                </div>
                <div class=\"col-sm-3\">
                     <form method='post' action =' " . Routes::ADMIN_DELETE_VIDEO_ACTION . " '>
                       <input type='hidden' name='id' value='" . $video['id'] . "'>
                      <button type ='action' class=\"btn btn-primary\" name=\"delete\">Verwijderen</button>
                    </form>
                </div>
               ";
				}
				?>
            </div><hr><a href="<?php echo Routes::ADMIN_OVERVIEW ?>" class="btn btn-primary float-left" role="button">
                Back
            </a><br><br><?php include_once "src/components/errorMessage.php"; ?>
        </div>
        <div class="col-sm-2"></div>
    </div>
</div>
</body>