<!DOCTYPE html>
<html lang="en">
<?php use src\enums\Routes;

include_once 'src/components/head.php'; ?>

</head>
<?php include_once 'src/components/navbar.php'; ?>
<?php include_once 'src/components/CheckIfAdmin.php' ?>
<body class=>
<div class="container">
	<head>
		<style>
			body {
				background: transparent;
			}
		</style>
	</head>
	<div class="text-center">
		<?php include_once 'src/components/banner.php'; ?>
		<div class="card" style="background-color: transparent">
			<div class="card-body">
				<h1>Add New User<br></h1>
				<form method="post" action="<?php echo Routes::ADMIN_REGISTER_ACTION ?>">
                    <div class="form-group"
                        <input name="redirect" type="hidden" class="form-control" id="pwd2" value="ADMIN_OVERVIEW">
                    </div>
					<div class="form-group">
						<label for="username"><h10>Username</h10></label>
						<input name="username" type="text" class="form-control" id="username">
					</div>
					<div class="form-group">
						<label for="email"><h10>E-mail</h10></label>
						<input name="email" type="email" class="form-control" id="email">
					</div>
					<div class="form-group">
						<label for="pwd"><h10>Password</h10></label>
						<input name="password1" type="password" class="form-control" id="pwd">
					</div>
					<div class="form-group">
						<label for="pwd2"><h10>Repeat password</h10></label>
						<input name="password2" type="password" class="form-control" id="pwd2">
					</div>
                    <div class="form-group">
                        <label for="sel1">Role</label>
                        <select class="form-control" id="sel1" name="role">
                            <option value="teacher" >teacher</option>
                            <option value="student" >student</option>
                            <option value="admin" >admin</option>
                        </select>
                    </div>
					<div>
						<button type="submit" class="btn btn-primary float-right">Submit</button>
                        <a href="<?php echo Routes::ADMIN_OVERVIEW ?>" class="btn btn-primary float-left" role="button">
                            Back
                        </a>
					</div>
				</form><br>
            <?php include_once "src/components/errorMessage.php"; ?>
			</div>
		</div>
	</div>
</div>
</body>
</html>