<!DOCTYPE html>
<html lang="en">

<?php

use src\controllers\VideoController;
use src\controllers\UserController;
use src\enums\Routes;

include_once 'src/components/head.php'; ?>
<?php include_once 'src/components/head.php'; ?>
<body>
<?php include_once 'src/components/navbar.php'; ?>
<?php include_once 'src/components/CheckIfAdmin.php' ?>
<div class="container-fluid">
    <head>
        <style>
            body {
                background: transparent;
            }
        </style>
    </head>
    <?php include_once 'src/components/banner.php'; ?>
    <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-8">
            <h1 class="text-center">Delete User</h1><hr>
            <div class="row">
                    <div class="col-sm-2">
                        ID
                    </div>
                    <div class="col-sm-3">
                        User Name
                    </div>
                    <div class="col-sm-3">
                        E-mail
                    </div>
                    <div class="col-sm-2">
                        Role
                    </div>
                    <div class="col-sm-2">
                        Delete
                    </div>
                    <div class="col-sm-12">
                        <hr style="background-color: white">
                    </div>
                </div>
            <div class="row">
			<?php
            $userController = new UserController();

			foreach ($userController->showAll() as $user) {
				echo "
                 <div class=\"col-sm-2\">
                    " . $user ['user_id'] . "
                </div>
                <div class=\"col-sm-3\">
                    " . $user ['username'] . "
                </div>
                <div class=\"col-sm-3\">
                    " . $user ['email'] . "
                </div>
                <div class='col-sm-2'> 
                    " .$user['role']. "
                </div>
                <div class=\"col-sm-2\">
                     <form method='post' action ='" . Routes::ADMIN_DELETE_USER_ACTION . "'>
                       <input type='hidden' name='id' value='" . $user['user_id'] . "'>
                      <button type ='action' class=\"btn btn-primary\" name=\"delete\">Delete</button>
                    </form>
                </div>
               ";
			}
			?>
            </div><hr><a href="<?php echo Routes::ADMIN_OVERVIEW ?>" class="btn btn-primary float-left" role="button">
                Back
            </a><br><br><?php include_once "src/components/errorMessage.php"; ?>
        </div>
        <div class="col-sm-2"></div>
    </div>
</div>
</body>