<?php

use src\enums\Routes;

?>
<!DOCTYPE html>
<html lang="en">
<?php include_once 'src/components/head.php'; ?>

<body>
<?php include_once 'src/components/navbar.php'; ?>
<div class="container">
    <head>
        <style>
            body {
                background: url();
            }
        </style>
    </head>
    <div class="text-center">
        <?php include_once 'src/components/banner.php'; ?>
        <div class="card" style="background-color: transparent">
            <div class="card-body">
                <h1>Register<br></h1>
                <h10>Choose if you want to register as teacher or student</h10>
                <br>
                <a href="<?php echo Routes::REGISTER_TEACHER ?>"><input type="button" value="Teacher"></a>
                <a href="<?php echo Routes::REGISTER_STUDENT ?>"><input type="button" value="Student"></a>
            </div>
        </div>
    </div>
</div>
</body>
</html>