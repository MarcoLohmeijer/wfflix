<?php

use src\controllers\HistoryController;
use src\enums\Routes;

$historyController = new HistoryController();
?>
<!DOCTYPE html>
<html lang="en">
<?php include_once 'src/components/head.php'; ?>
<body class=>
<?php include_once 'src/components/navbar.php'; ?>
<div class="container-fluid">
    <head>
        <style>
            body {
                background: url();
            }
        </style>
    </head>
    <div class="text-center">
        <?php include_once 'src/components/banner.php'; ?>
        <div class="card">
            <div class="row">
                <div class="col-sm-2"></div>
                <div class="col-sm-8">
                    <h1>Your watch history</h1>
                    <form action="<?php echo Routes::DELETE_HISTORY_ACTION?>" method="post">
                        <input type="hidden" name="user_id", value="<?php echo $_SESSION['user_id']?>">
                        <button type="submit" class="btn btn-primary">Delete</button>
                    </form>
                    <br>
                    <div class="row">
                        <div class="col-sm-2">
                            HistoryID
                        </div>
                        <div class="col-sm-2">
                            VideoID
                        </div>
                        <div class="col-sm-5">
                            Videoname
                        </div>
                        <div class="col-sm-3">
                            Date
                        </div>
                    </div><hr style="background-color: white">

                    <?php

                    foreach ($historyController->showHistory($_SESSION['user_id']) as $history) {
                        echo "
                         <div class='row' >
                             <div class=\"col-sm-2\">
                                " . $history ['id'] . "
                            </div>
                            <div class=\"col-sm-2\">
                                " . $history ['video_id'] . "
                            </div>
                                <div class=\"col-sm-5\">
                                " . $history ['title'] . "
                            </div>
                            <div class=\"col-sm-3\">
                                " . $history ['date'] . "
                            </div>
                         </div>";
                    }
                    ?><?php include_once "src/components/errorMessage.php"; ?><br>
                    </div>
            </div>
        </div>
    </div>
</div>
</body>

</html>