<!DOCTYPE html>
<html lang="en">

<?php

use src\controllers\VideoController;
use src\enums\Routes;

include_once 'src/components/head.php';
$videoController = new VideoController();?>

<body class="bg-transparent">
<?php include_once 'src/components/navbar.php'; ?>
<?php include_once 'src/components/banner.php'; ?>
<div class="container-fluid">
    <head>
        <style>
            body {
                background: transparent;
            }
        </style>
    </head>
    <div class="row">
        <div class="col-sm-3"></div>
        <div class="col-sm-6">
            <h4>My Playlist Content</h4><hr style="background-color: white">
            <div class="row">
            <?php
            foreach ($videoController->showPlaylistContent($_GET['playlist_id']) as $video) {
                echo "
                    <div class=\"col-sm-4\">
                        <a class=\"video-card-link\" href=\"" . Routes::WATCH_VID_PLAYLIST . "&video=" . $video['video_url'] . "&id=" . $video['id'] . "&playlist_id=" . $video['playlist_id'] ." \">
                            <div class=\"card-deck\">
                                <div class=\"card\" >
                                    <div class=\"card-body1\">
                                    <img class=\"card-img-top\" src=\"src/thumbnails/" . $video['thumbnail'] . "\" alt=\"Card image\" style=\"width:100%\">
                                        <h4 class=\"card-title\">" . $video['title'] . "</h4>
                                        <p class=\"card-text\">" . substr($video['description'], 0, 80) . "</p>
                                        <p class=\"card-text float-right\">Views: " . $video['views'] . "</p>
                                        <p class=\"card-text\">Likes: " . $video['likes'] . "</p>
                                        <small class=\"card-text\">" . $video['created_at'] . "</small>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <form action='".Routes::DELETE_FROM_PLAYLIST."' method='post'>
                            <input type='hidden' name='vid_id' value='".$video['id']."'>
                            <input type='hidden' name='playlist_id' value='".$_GET['playlist_id']."'>
                            <button type='submit' class='btn btn-primary'>Delete from playlist</button>
                        </form>
                    </div>";
            }
            ?>
        </div>
        </div>
        <div class="col-sm-3"></div>

