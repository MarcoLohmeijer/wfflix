<?php
use src\controllers\TagController;
use src\enums\Routes;
include_once 'src/components/checkRole.php';
?>
<!DOCTYPE html>
<html lang="en">
<?php include_once 'src/components/head.php'; ?>
<body>
<?php include_once 'src/components/navbar.php'; ?>

<div class="container-fluid" style="margin-top: 30px">
    <head>
        <style>
            body {
                background: transparent;
            }
        </style>
    </head>
    <?php include_once 'src/components/banner.php'; ?>
    <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-8">
            <h1 class="text-center">Video upload</h1>
        </div>
        <div class="col-sm-2"></div>
    </div>
    <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-8">
            <div class="row">
                <div class="col-sm-3"></div>
                <div class="col-sm-6">
            <form action="<?php echo Routes::UPLOAD_ACTION ?>" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="video" style="color: white">Video:</label>
                    <input type="file" class="form-control-file border" name="video" id="video">
                </div>

                <div class="form-group">
                    <label for="thumbnail" style="color: white">Thumbnail:</label>
                    <input type="file" class="form-control-file border" name="thumbnail" id="thumbnail">
                </div>

                <div class="form-group">
                    <label for="title" style="color: white">Title:</label>
                    <input type="text" class="form-control" name="title" id="title">
                </div>

                <div class="form-group">
                    <label for="comment" style="color: white">Comment:</label>
                    <input type="text" class="form-control" name="description" id="comment">
                </div>
                <input type="text" id="input" onkeyup="myFunction()" placeholder="Search for tags"
                       title="Type in a name">
                <div class="col-sm-6">
                    <div class="scrollList row">
                        <ul id="ul" class="list-group">
                            <?php

                            $tagController = new TagController();
                            $tags = $tagController->showAll();

                            foreach ($tags as $tag) {
                                echo "<li><input type='checkbox' name='tags_list[]' value=" . $tag['id'] . "><text>" . $tag['tag'] . "</text></li>";
                            } ?>

                        </ul>
                    </div>
                </div>
                <script src="src/js/searchTags.js"></script>
                <button type="submit" class="btn btn-primary btn-block">Verzend</button><hr>
            </form>
                    <?php include_once "src/components/errorMessage.php"; ?>
            </div>
                <div class="col-sm-3"></div>
            </div>
        </div>
    </div>
    <div class="col-sm-2"></div>
</div>
</div>
</body>
</html>
