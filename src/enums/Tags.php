<?php

namespace src\enums;

class Tags
{
    const CSS = 'CSS';
    const JAVA = 'JAVA';
    const Csharp = 'C#';
    const PHP = 'PHP';
}