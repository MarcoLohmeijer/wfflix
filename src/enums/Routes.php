<?php

namespace src\enums;

class Routes
{
    /** views */
    const HOME = '?uri=home';
    const LOGIN = '?uri=login';
    const REGISTER = '?uri=register';
    const LOGOUT = '?uri=logout';
    const REGISTER_STUDENT = '?uri=register-student';
    const REGISTER_TEACHER = '?uri=register-teacher';
    const WATCH = '?uri=watch';
    const UPLOAD = '?uri=upload';
    const UPDATE_USER = '?uri=update-user';
    const UPDATE_PASSWORD = '?uri=change-password';
    const MY_UPLOADS = '?uri=my-uploads';
    const MY_PLAYLISTS = '?uri=my-playlists';
    const CREATE_PLAYLIST = '?uri=create-playlist';
    const USER_HISTORY = '?uri=user-history';
    const USER_COMMENTS = '?uri=user-comments';
    const PLAYLIST_OVERVIEW_VIEW = '?uri=playlist-overview';
    const WATCH_VID_PLAYLIST = '?uri=watch-video-playlist';
    const WATCH_PLAYLIST_CONTENT_REDIRECT = '?uri=playlist-content';
    const EDIT_VIDEO_VIEW =  '?uri=edit-video';

    /** Uri voor get input */
    const PLAYLIST_OVERVIEW = 'playlist-overview';
    const WATCH_PLAYLIST_CONTENT = 'playlist-content';
    const EDIT_VIDEO =  'edit-video';

    /** actions */
    const LOGIN_ACTION = '?uri=login-action';
    const REGISTER_ACTION = '?uri=action/create_user';
    const UPLOAD_ACTION = '?uri=actions/upload-video';
    const LIKE_VIDEO_ACTION = '?uri=actions/like-video';
    const UPDATE_USER_ACTION = '?uri=update-user-action';
    const UPDATE_PASSWORD_ACTION = '?uri=update-password-action';
    const CREATE_PLAYLIST_ACTION = '?uri=create-playlist-action';
    const COMMENT_VIDEO = '?uri=comment-video';
    const ADD_TO_PLAYLIST_ACTION = '?uri=addToPlaylistAction';
    const DELETE_COMMENT_ACTION = '?uri=delete-comment';
    const DELETE_HISTORY_ACTION = '?uri=delete-history-action';
    const DELETE_FROM_PLAYLIST ='?uri=delete-from-playlist-action';
    const DELETE_PLAYLIST = '?uri=delete-playlist';
    const EDIT_VIDEO_ACTION = '?uri=edit-video-action';
    const DELETE_VIDEO_ACTION = '?uri=delete-video-action';

    /** admin views */
	const ADMIN_OVERVIEW = '?uri=admin-overview';
	const ADMIN_ADD_USER = '?uri=admin-add-user';
	const ADMIN_SHOW_USERS = '?uri=admin-show-users';
	const ADMIN_SHOW_VIDEOS = '?uri=admin-show-videos';

    /** admin actions */
	const ADMIN_REGISTER_ACTION= '?uri=admin-register-action';
	const ADMIN_DELETE_USER_ACTION = '?uri=actions/admin-delete-user';
	const ADMIN_DELETE_VIDEO_ACTION = '?uri=actions/admin-delete-video';
}