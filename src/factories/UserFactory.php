<?php

namespace src\factories;

use src\models\User;

include_once "src/models/User.php";

class UserFactory
{
    /**
     * @param $username
     * @param $email
     * @param $password
     * @param $role
     *
     * @return User
     */
    public static function create($username, $email, $password, $role)
    {
        $user = new User;
        $user->setUsername($username);
        $user->setEmail($email);
        $user->setPassword($password);
        $user->setRole($role);

        return $user;
    }
}