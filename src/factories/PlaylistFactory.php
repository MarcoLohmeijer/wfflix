<?php

namespace src\factories;

use src\models\Playlist;

class PlaylistFactory
{
    /**
     * @param $name
     * @param $user_id
     * @return Playlist
     */
    public static function create($name, $user_id)
    {
        $playlist = new playlist();
        $playlist->setName($name);
        $playlist->setUserId($user_id);

        return $playlist;
    }
}