<?php

namespace src\factories;

use src\models\History;

class HistoryFactory
{
    /**
     * @param $user_id
     * @param $video_id
     * @return History
     * @throws \Exception
     */
    public static function create($user_id, $video_id)
    {
        $history = new History();
        $date = new \DateTime();

        $history->setUserId($user_id);
        $history->setVideoId($video_id);
        $history->setDate($date->format("Y-m-d H:i:s"));

        return $history;
    }
}