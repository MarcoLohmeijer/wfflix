<?php


namespace src\factories;


use DateTime;
use Exception;
use src\models\Video;

include_once "src/models/Video.php";

class VideoFactory
{
    /**
     * @param string $title
     * @param string $description
     * @param string $video_url
     * @param int $user_id
     * @param $thumbnail_url
     *
     * @return Video
     * @throws Exception
     */
    public static function create($title, $description, $video_url, $user_id, $thumbnail_url)
    {
        $date = new DateTime();
        $video = new Video();

        $video->setTitle($title);
        $video->setDescription($description);
        $video->setVideoUrl($video_url);
        $video->setCreatedAt($date->format('Y-m-d'));
        $video->setUserId($user_id);
        $video->setThumbnailUrl($thumbnail_url);

        return $video;
    }

    /**
     * @param $title
     * @param $description
     *
     * @return Video
     */
    public static function editVideo($title, $description)
    {
        $video = new Video();

        $video->setTitle($title);
        $video->setDescription($description);

        return $video;
    }
}