<?php

namespace src\factories;

use DateTime;
use Exception;
use src\models\Comment;

class CommentFactory
{
    /**
     * @param $comment_input
     * @param $userId
     * @param int $videoId
     * @return comment
     * @throws Exception
     */
    public static function insert($comment_input,  $userId,  $videoId)
    {
        $comment = new Comment();
        $date = new DateTime();

        $comment->setComment($comment_input);
        $comment->setUserid($userId);
        $comment->setVideoId($videoId);
        $comment->setCreatedAt($date->format('Y-m-d'));

        return $comment;
    }
}