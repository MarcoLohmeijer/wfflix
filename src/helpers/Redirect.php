<?php

namespace src\helpers;

use src\enums\App;

class Redirect
{
    /**
     * @param string $uri
     * @param null $get
     */
    public static function to($uri, $get = null)
    {
        if (isset($get)) {
            header("Location: ". App::URL . App::SERVER_URI . $uri ."&". $get);
            exit;
        }

        header("Location: ". App::URL . App::SERVER_URI . $uri);
        exit;
    }
}