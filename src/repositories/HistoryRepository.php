<?php

namespace src\repositories;

use Exception;
use PDO;
use PDOException;

class HistoryRepository
{
    /** @var PDO */
    private $conn;

    /**
     * UserRepository constructor.
     * @param PDO $conn
     */
    public function __construct(PDO $conn)
    {
        $this->conn = $conn;
    }

    /**
     * @param $data
     *
     * @return bool|string
     * @throws Exception
     */
    public function create($data)
    {
        try {
            $stmt = $this->conn->prepare("INSERT INTO history(user_id, video_id, date)
                                                VALUES(:user_id, :video_id, :date)");
            $stmt->bindParam(':user_id', $data->userId, PDO::PARAM_INT);
            $stmt->bindParam(':video_id', $data->videoId, PDO::PARAM_INT);
            $stmt->bindParam(':date', $data->date, PDO::PARAM_STR);

            return $stmt->execute();
        } catch (PDOException $e) {
            throw new Exception("error: " . $e->getMessage());
        }
    }

    /**
     * @param $user_id
     *
     * @return array
     * @throws Exception
     */
    public function showHistory($user_id)
    {
        try {
            $stmt = $this->conn->prepare("SELECT history.id, history.video_id, videos.title, history.date
                                                    FROM history 
                                                    INNER JOIN videos ON history.video_id = videos.id
                                                    WHERE history.user_id = :user_id
                                                    ORDER BY date DESC ");
            $stmt->bindParam(':user_id', $user_id, PDO::PARAM_INT);
            $stmt->execute();
            $stmt->setFetchMode(PDO::FETCH_ASSOC);

            return $stmt->fetchAll();
        } catch (PDOException $e) {
            throw new Exception("Error: " . $e->getMessage());
        }
    }

    /**
     * @param $user_id
     *
     * @return bool|string
     * @throws Exception
     */
    public function deleteHistory($user_id)
    {
        try {
            $stmt = $this->conn->prepare("DELETE FROM history WHERE user_id = :user_id");
            $stmt->bindParam(':user_id', $user_id, PDO::PARAM_INT);

            return $stmt->execute();
        } catch (PDOException $e) {
            throw new Exception("error: " . $e->getMessage());
        }
    }

    /**
     * @param $user_id
     *
     * @return array
     * @throws Exception
     */
    public function getById($user_id)
    {
        try {
            $stmt = $this->conn->prepare("SELECT * FROM history WHERE user_id = :user_id");
            $stmt->bindParam(':user_id', $user_id, PDO::PARAM_INT);

            $stmt->execute();

            return $stmt->fetchAll(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            throw new Exception("Error: " . $e->getMessage());
        }
    }
}