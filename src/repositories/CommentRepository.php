<?php


namespace src\repositories;

use Exception;
use PDO;
use PDOException;
use src\models\Comment;

class CommentRepository
{
    /**
     * @var PDO
     */

    private $conn;

    /**
     * CommentRepository constructor.
     * @param PDO $conn
     */

    public function __construct(PDO $conn)
    {
        $this->conn = $conn;
    }

    /**
     * @param Comment $comment
     *
     * @return mixed
     * @throws Exception
     */

    public function insert(Comment $comment)
    {
        try {
            $stmt = $this->conn->prepare("INSERT INTO comments(comment, user_id, video_id, created_at)
                                                VALUES(:comment_content, :user_id, :video_id, :created_at)");

            $stmt->bindParam(':video_id', $comment->getVideoId(), PDO::PARAM_INT);
            $stmt->bindParam(':user_id', $comment->getUserId(), PDO::PARAM_INT);
            $stmt->bindParam(':comment_content', $comment->getComment(), PDO::PARAM_STR);
            $stmt->bindParam(':created_at', $comment->getCreatedAt(), PDO::PARAM_STR);

            return $stmt->execute();
        } catch (PDOException $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * @param $userId
     *
     * @return array|string
     * @throws Exception
     */
    public function showUserComment($userId)
    {
        try {
            $stmt = $this->conn->prepare("SELECT * FROM comments WHERE user_id= :user_id");
            $stmt->bindParam(':user_id', $userId, PDO::PARAM_INT);

            $stmt->execute();
            $stmt->setFetchMode(PDO::FETCH_ASSOC);

            return $stmt->fetchAll();
        } catch (PDOException $e) {
            throw new Exception("Error: " . $e->getMessage());
        }
    }

    /**
     * @param $videoId
     *
     * @return array
     * @throws Exception
     */
    public function showComment($videoId)
    {
        try {
            $stmt = $this->conn->prepare("SELECT * FROM comments
 													INNER JOIN users u on comments.user_id = u.id
 													WHERE video_id= :video_id");
            $stmt->bindParam('video_id', $videoId, PDO::PARAM_INT);

            $stmt->execute();
            $stmt->setFetchMode(PDO::FETCH_ASSOC);

            return $stmt->fetchAll();
        } catch (PDOException $e) {
            throw new Exception("Error: " . $e->getMessage());
        }
    }

    /**
     * @param $comment_id
     *
     * @return bool
     * @throws Exception
     */
    public function delete($comment_id)
    {
        try {
            $stmt = $this->conn->prepare("DELETE FROM comments WHERE id = :id");
            $stmt->bindParam(':id', $comment_id, PDO::PARAM_INT);

            return $stmt->execute();
        } catch (PDOException $e) {
            throw new Exception("Error: " . $e->getMessage());
        }
    }
}