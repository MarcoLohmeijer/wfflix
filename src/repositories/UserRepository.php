<?php

namespace src\repositories;

use Exception;
use PDO;
use PDOException;
use src\enums\Routes;
use src\helpers\Redirect;
use src\models\User;

class UserRepository
{
    /** @var PDO */
    private $conn;

    /**
     * UserRepository constructor.
     * @param PDO $conn
     */
    public function __construct(PDO $conn)
    {
        $this->conn = $conn;
    }

    /**
     * @param $user
     * @param $id
     *
     * @return bool
     * @throws Exception
     */
    public function UpdateUserPassword(User $user, $id)
    {
        try {
            $stmt = $this->conn->prepare("UPDATE users SET password = :password
                                                   WHERE id = :id");
            $stmt->bindParam(':password', $user->getPassword(), PDO::PARAM_STR);
            $stmt->bindParam(':id', $id, PDO::PARAM_INT);

            return $stmt->execute();
        } catch (PDOException $e) {
            throw new Exception("error: " . $e->getMessage());
        }
    }

    /**
     * @param $username
     *
     * @return mixed
     * @throws Exception
     */
    public function getByUsername($username)
    {
        try {
            $stmt = $this->conn->prepare("SELECT * FROM users 
                                            JOIN roles r on users.id = r.user_id
                                            WHERE username =:username");
            $stmt->bindParam(':username', $username, PDO::PARAM_STR);

            $stmt->execute();
            return $stmt->fetch(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            throw new Exception("error: " . $e->getMessage());
        }
    }

    /**
     * @param $email
     *
     * @return mixed
     * @throws Exception
     */
    public function getByEmail($email)
    {
        try {
            $stmt = $this->conn->prepare("SELECT * FROM users WHERE email =:email");
            $stmt->bindParam(':email', $email, PDO::PARAM_STR);

            $stmt->execute();

            return $stmt->fetch(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            throw new Exception("error: " . $e->getMessage());
        }
    }

    /**
     * @param $username
     *
     * @return bool
     * @throws Exception
     */
    public function deleteByUsername($username)
    {
        try {
            $stmt = $this->conn->prepare("DELETE FROM users WHERE username = :username");
            $stmt->bindParam(':username', $username, PDO::PARAM_STR);

            return $stmt->execute();
        } catch (PDOException $e) {
            throw new Exception("error: " . $e->getMessage());
        }
    }

	/**
	 * @param $id
	 *
	 * @return bool|string
	 */
	public function deleteById($id)
	{
		try {
			$del = $this->conn->prepare("DELETE FROM comments WHERE user_id= :id;
													DELETE FROM history WHERE user_id= :id;
													DELETE FROM playlists WHERE user_id = :id;
													DELETE FROM roles WHERE user_id = :id;
													DELETE FROM videos WHERE user_id :id;
													DELETE FROM users WHERE id = :id");
			$del->bindParam(':id',$id, PDO::PARAM_STR);
			return $del->execute();

		} catch (PDOException $e) {
			return "error: " . $e->getMessage();
		}
	}

    /**
     * @param $id
     * @param $role
     *
     * @return bool
     * @throws Exception
     */
    public function setRole($id, $role)
    {
        try {
            $stmt = $this->conn->prepare("INSERT INTO roles (role, user_id) 
                                            VALUES (:role, :user_id)");
            $stmt->bindParam(':role', $role, PDO::PARAM_STR);
            $stmt->bindParam(':user_id', $id, PDO::PARAM_INT);

            return $stmt->execute();
        } catch (PDOException $e) {
            throw new Exception("error: " . $e->getMessage());
        }
    }

    /**
     * @param User $user
     *
     * @return string
     * @throws Exception
     */
    public function create(User $user)
    {
        try {
            $stmt = $this->conn->prepare("INSERT INTO users(username, password, email)
                                                VALUES(:username, :password, :email)");

            $stmt->bindParam(':username', $user->getUsername());
            $stmt->bindParam(':password', $user->getPassword());
            $stmt->bindParam(':email', $user->getEmail());
            $stmt->execute();

            return $this->conn->lastInsertId();
        } catch (PDOException $e) {
            throw new Exception("error: " . $e->getMessage());
        }
    }

    /**
     * @param User $user
     * @param $id
     *
     * @return bool
     * @throws Exception
     */
    public function update(User $user, $id)
    {
        try {
            $stmt = $this->conn->prepare("UPDATE users SET username = :username, email = :email 
                                                    WHERE id = :id");
            $stmt->bindParam(':username', $user->getUsername(), PDO::PARAM_STR);
            $stmt->bindParam(':email', $user->getEmail(), PDO::PARAM_STR);
            $stmt->bindParam(':id', $id, PDO::PARAM_INT);

            return $stmt->execute();
        } catch (PDOException $e) {
            throw new Exception("error: " . $e->getMessage());
        }
    }

    /**
     * @param $id
     *
     * @return bool|string
     * @throws Exception
     */
    public function delete($id)
    {
        try {
            $stmt = $this->conn->prepare("DELETE FROM users WHERE id = :id");
            $stmt->bindParam(':id', $id, PDO::PARAM_INT);

            return $stmt->execute();
        } catch (PDOException $e) {
            throw new Exception("error: " . $e->getMessage());
        }
    }

    /**
     * @return mixed|string
     * @throws Exception
     */
    public function getAll()
    {
        try {
            $stmt = $this->conn->prepare("SELECT * FROM users 
                                            JOIN roles r on users.id = r.user_id");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            throw new Exception("error: " . $e->getMessage());
        }
    }

    /**
     * @param $id
     *
     * @return mixed|string
     * @throws Exception
     */
    public function getOne($id)
    {
        try {
            $stmt = $this->conn->prepare("SELECT * FROM users 
                                            JOIN roles r on users.id = r.user_id
                                            WHERE id =:id");

            $stmt->bindParam(':id', $id, PDO::PARAM_INT);
            $stmt->execute();

            return $stmt->fetch(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            throw new Exception("error: " . $e->getMessage());
        }
    }
}