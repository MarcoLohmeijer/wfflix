<?php


namespace src\repositories;

use Exception;
use PDO;
use PDOException;
use src\models\Playlist;

class PlaylistRepository
{
    /** @var PDO */
    private $conn;

    /**
     * Connection to database
     * @param PDO $conn
     */
    public function __construct(PDO $conn)
    {
        $this->conn = $conn;
    }

    /**
     * @param Playlist $playlist
     *
     * @return bool|string
     * @throws Exception
     */
    public function create(Playlist $playlist)
    {
        try {
            $stmt = $this->conn->prepare("INSERT INTO playlists(name, user_id)
                                                VALUES(:title, :user_id)");
            $stmt->bindParam(':title', $playlist->getName(), PDO::PARAM_STR);
            $stmt->bindParam(':user_id', $playlist->getUserId(), PDO::PARAM_INT);

            return $stmt->execute();
        } catch (PDOException $e) {
            throw new Exception("error: " . $e->getMessage());
        }
    }

    /**
     * @param $vid_id
     * @param $playlist_id
     *
     * @return bool
     * @throws Exception
     */
    public function add($vid_id, $playlist_id)
    {
        try {
            $stmt = $this->conn->prepare("INSERT INTO video_playlist(video_id, playlist_id)
                                                VALUES(:title, :user_id)");
            $stmt->bindParam(':title', $vid_id, PDO::PARAM_INT);
            $stmt->bindParam(':user_id', $playlist_id, PDO::PARAM_INT);

            return $stmt->execute();
        } catch (PDOException $e) {
            throw new Exception("error: " . $e->getMessage());
        }
    }

    /**
     * @param $playlist_id
     *
     * @return mixed
     * @throws Exception
     */
	public function getOne($playlist_id)
	{
		try{
			$stmt = $this->conn->prepare("SELECT * FROM playlists WHERE id=:id");
			$stmt->bindParam(":id", $playlist_id, PDO::PARAM_INT);
			$stmt->execute();

			return $stmt->fetch(PDO::FETCH_ASSOC);
		} catch (PDOException $e) {
			throw new Exception("error: " . $e->getMessage());
		}
	}

	/**
	 * @param $playlist_id
	 * @param $limit
	 * @param $currentVideo
	 *
	 * @return array
	 * @throws Exception
	 */
	public function showNextOfPlaylistLimited($playlist_id, $limit, $currentVideo)
	{
        try{
            $stmt = $this->conn->prepare("SELECT videos.id, videos.title, videos.description, videos.video_url, 
													videos.created_at, videos.views, videos.likes, videos.user_id, thumbnail, 
													vp.id, vp.video_id, playlist_id, p.id, p.name, p.user_id FROM wfflix.videos
													INNER JOIN video_playlist vp on videos.id = vp.video_id
													INNER JOIN playlists p on vp.playlist_id = p.id
													WHERE vp.id>:currentVideo AND playlist_id = :id LIMIT :limit");

            $stmt->bindParam(":id", $playlist_id, PDO::PARAM_INT);
			$stmt->bindParam(":limit", $limit, PDO::PARAM_INT);
			$stmt->bindParam(":currentVideo", $currentVideo['id'], PDO::PARAM_INT);
            $stmt->execute();

            return $stmt->fetchAll(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
	throw new Exception("error: " . $e->getMessage());
        }
    }

	/**
	 * @param $videoId
	 *
	 * @return array
	 * @throws Exception
	 */
    public function showCurrentVideoInPlaylist($videoId)
	{
		try{
			$stmt = $this->conn->prepare("SELECT video_playlist.id FROM wfflix.video_playlist
													INNER JOIN videos v on video_playlist.video_id = v.id
													WHERE v.id=:id");

			$stmt->bindParam(":id", $videoId, PDO::PARAM_INT);
			$stmt->execute();

			return $stmt->fetch(PDO::FETCH_ASSOC);
		} catch (PDOException $e) {
			throw new Exception("error: " . $e->getMessage());
		}
	}

}
