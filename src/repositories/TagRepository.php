<?php

namespace src\repositories;

use Exception;
use PDO;
use PDOException;

class TagRepository
{
	/** @var PDO */
	private $conn;

	/**
	 * UserRepository constructor.
	 * @param PDO $conn
	 */
	public function __construct(PDO $conn)
	{
		$this->conn = $conn;
	}

    /**
     * @return array
     * @throws Exception
     */
	public function selectAll()
	{
		try {
			$stmt = $this->conn->prepare("SELECT * FROM tags");
			$stmt->execute();

			return $stmt->fetchAll(PDO::FETCH_ASSOC);
		} catch (PDOException $e) {
			throw new Exception( "error: " . $e->getMessage());
		}
	}

    /**
     * @param $tag
     * @param $vid_id
     *
     * @return string
     * @throws Exception
     */
	public function saveTag($tag, $vid_id)
	{
		try {
			$stmt = $this->conn->prepare("INSERT INTO video_tags (tag_id, video_id)
													VALUES (:tag_id, :vid_id)");
			$stmt->bindParam(':tag_id', $tag, PDO::PARAM_INT);
			$stmt->bindParam(':vid_id', $vid_id, PDO::PARAM_INT);

			return $stmt->execute();
		} catch (PDOException $e) {
			throw new Exception("error: " . $e->getMessage());
		}
	}

    /**
     * @param $vid_id
     *
     * @return array|string
     * @throws Exception
     */
	public function showTagVideo($vid_id)
	{
		try {
			$stmt = $this->conn->prepare("SELECT tag FROM tags
 													INNER JOIN video_tags ON tags.id = video_tags.tag_id
 													WHERE video_id = :vid_id");
			$stmt->bindParam(':vid_id', $vid_id, PDO::PARAM_INT);

			$stmt->execute();

			return $stmt->fetchAll(PDO::FETCH_ASSOC);
		} catch (PDOException $e) {
			throw new Exception("error: " . $e->getMessage());
		}
	}

    /**
     * @param $video_id
     * @return array
     * @throws Exception
     */
	public function getByHistory($video_id)
    {
        try {
            $stmt = $this->conn->prepare("SELECT tag FROM tags
 													INNER JOIN video_tags ON tags.id = video_tags.tag_id
 													WHERE video_id = :video_id");

            $stmt->bindParam(':video_id', $video_id, PDO::PARAM_INT);
            $stmt->execute();

            return $stmt->fetchAll(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            throw new Exception("error: " . $e->getMessage());
        }
    }
}

