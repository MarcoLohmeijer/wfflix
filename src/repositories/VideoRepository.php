<?php

namespace src\repositories;

use Exception;
use PDO;
use PDOException;
use src\models\Video;

class VideoRepository
{
    /** @var PDO */
    private $conn;

    /**
     * Connection to database
     * @param PDO $conn
     */
    public function __construct($conn)
    {
        $this->conn = $conn;
    }

    /**
     * @return mixed|string
     * @throws Exception
     */
    public function getAll()
    {
        try {
            $stmt = $this->conn->prepare("SELECT * FROM videos");
            $stmt->execute();
            $stmt->setFetchMode(PDO::FETCH_ASSOC);

            return $stmt->fetchAll();
        } catch (PDOException $e) {
            throw new Exception("Error: " . $e->getMessage());
        }
    }

    /**
     * @param $user_id
     *
     * @return array|string
     * @throws Exception
     */
    public function getUserUploads($user_id)
    {
        try {
            $stmt = $this->conn->prepare("SELECT * FROM videos WHERE user_id= :user_id");
            $stmt->bindParam(':user_id', $user_id, PDO::PARAM_INT);

            $stmt->execute();
            $stmt->setFetchMode(PDO::FETCH_ASSOC);

            return $stmt->fetchAll();

        } catch (PDOException $e) {
            throw new Exception("Error: " . $e->getMessage());
        }
    }

    /**
     * @param $user_id
     *
     * @return array
     * @throws Exception
     */
    public function getUserPlaylists($user_id)
    {
        try {
            $stmt = $this->conn->prepare("SELECT * FROM playlists WHERE user_id= :user_id");
            $stmt->bindParam(':user_id', $user_id, PDO::PARAM_INT);

            $stmt->execute();
            $stmt->setFetchMode(PDO::FETCH_ASSOC);

            return $stmt->fetchAll();

        } catch (PDOException $e) {
            throw new Exception("Error: " . $e->getMessage());
        }
    }

    /**
     * @return mixed|string
     * @throws Exception
     */
    public function getWithLimit()
    {
        try {
            $stmt = $this->conn->prepare("SELECT * FROM videos LIMIT 4");
            $stmt->execute();
            $stmt->setFetchMode(PDO::FETCH_ASSOC);

            return $stmt->fetchAll();
        } catch (PDOException $e) {
            throw new Exception("Error: " . $e->getMessage());
        }
    }

    /**
     * @param $id
     *
     * @return mixed|string
     * @throws Exception
     */
    public function getWatchedSuggestions($id)
    {
        try {
            $stmt = $this->conn->prepare("SELECT * FROM videos WHERE NOT id=:id LIMIT 4");
            $stmt->bindParam(':id', $id, PDO::PARAM_INT);

            $stmt->execute();
            $stmt->setFetchMode(PDO::FETCH_ASSOC);

            return $stmt->fetchAll();
        } catch (PDOException $e) {
            throw new Exception("Error: " . $e->getMessage());
        }
    }

    /**
     * @param $id
     * @param $views
     *
     * @return bool|string
     * @throws Exception
     */
    public function updateViews($id, $views)
    {
        try {
            $stmt = $this->conn->prepare("UPDATE videos SET views = :views WHERE id=:id");
            $stmt->bindParam(':id', $id, PDO::PARAM_INT);
            $stmt->bindParam(':views', $views, PDO::PARAM_INT);

            return $stmt->execute();
        } catch (PDOException $e) {
            throw new Exception("Error: " . $e->getMessage());
        }
    }

    /**
     * @param Video $video
     *
     * @return string
     * @throws Exception
     */
    public function create(Video $video)
    {
        try {
            $stmt = $this->conn->prepare("INSERT INTO videos(title, description, video_url, created_at, views, likes, user_id, thumbnail)
                                                VALUES(:title, :description, :video_url, :created_at, :views, :likes, :user_id, :thumbnail)");

            $stmt->bindParam(':title', $video->getTitle(), PDO::PARAM_STR);
            $stmt->bindParam(':description', $video->getDescription(), PDO::PARAM_STR);
            $stmt->bindParam(':video_url', $video->getVideoUrl(), PDO::PARAM_STR);
            $stmt->bindParam(':created_at', $video->getCreatedAt(), PDO::PARAM_STR);
            $stmt->bindParam(':views', $video->getViews(), PDO::PARAM_INT);
            $stmt->bindParam(':likes', $video->getLikes(), PDO::PARAM_INT);
            $stmt->bindParam(':user_id', $video->getUserId(), PDO::PARAM_INT);
            $stmt->bindParam(':thumbnail', $video->getThumbnailUrl(), PDO::PARAM_STR);

            $stmt->execute();

            return $this->conn->lastInsertId();
        } catch (PDOException $e) {
            throw new Exception("Error: " . $e->getMessage());
        }
    }

    /**
     * @param $id
     *
     * @return mixed
     * @throws Exception
     */
    public function getOne($id)
    {
        try {
            $stmt = $this->conn->prepare("SELECT * FROM videos 
                                                    JOIN users u on videos.user_id = u.id 
                                                    WHERE videos.id = :id");
            $stmt->bindParam(':id', $id, PDO::PARAM_INT);

            $stmt->execute();

            return $stmt->fetch(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            throw new Exception("Error: " . $e->getMessage());
        }
    }

    /**
     * @param $id
     * @param $likes
     *
     * @return bool|string
     * @throws Exception
     */

    public function updateLikes($id, $likes)
    {
        try {
            $stmt = $this->conn->prepare("UPDATE videos SET likes = :likes WHERE id = :id");
            $stmt->bindParam(':likes', $likes, PDO::PARAM_INT);
            $stmt->bindParam(':id', $id, PDO::PARAM_INT);

            return $stmt->execute();
        } catch (PDOException $e) {
            throw new Exception("Error: " . $e->getMessage());
        }
    }

    /**
     * @param $userId
     * @param $videoId
     *
     * @return bool
     * @throws Exception
     */
    public function updateLikeDetails($userId, $videoId)
    {
        try {
            $stmt = $this->conn->prepare("INSERT INTO likedetails (videoid, userid)
                                                    VALUES (:videoId, :userId)");
            $stmt->bindParam(':videoId', $videoId, PDO::PARAM_INT);
            $stmt->bindParam(':userId', $userId, PDO::PARAM_INT);

            return $stmt->execute();
        } catch (PDOException $e) {
            throw new Exception("Error: " . $e->getMessage());
        }
    }

    /**
     * @param $userId
     * @param $videoId
     *
     * @return array
     * @throws Exception
     */
    public function checkLikeDetails($userId, $videoId)
    {
        try {
            $stmt = $this->conn->prepare("SELECT * FROM likedetails  
                                            WHERE videoid = :videoId AND userid = :userId");

            $stmt->bindParam(':videoId', $videoId, PDO::PARAM_INT);
            $stmt->bindParam(':userId', $userId, PDO::PARAM_INT);

            $stmt->execute();

            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {
            throw new Exception("Error: " . $e->getMessage());
        }
    }

    /**
     * @param $max
     * @param $tags
     *
     * @return array
     * @throws Exception
     */
    public function getSuggested($max, $tags)
    {
        try {
            $stmt = $this->conn->prepare("SELECT DISTINCT videos.video_url, videos.thumbnail, videos.title, 
                                            videos.description, videos.views, videos.likes, videos.created_at, videos.id 
                                            FROM videos
                                            INNER JOIN video_tags vt on videos.id = vt.video_id
                                            INNER JOIN tags t on vt.tag_id = t.id
                                            WHERE t.tag IN (:tag1, :tag2, :tag3)
                                            LIMIT :max");

            $stmt->bindParam(':tag1', $tags[0], PDO::PARAM_STR);
            $stmt->bindParam(':tag2', $tags[1], PDO::PARAM_STR);
            $stmt->bindParam(':tag3', $tags[2], PDO::PARAM_STR);
            $stmt->bindParam(':max', $max, PDO::PARAM_INT);

            $stmt->execute();

            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * @param $id
     * @return bool
     * @throws Exception
     */
    public function delete($id)
    {
        try {
            $stmt = $this->conn->prepare("DELETE FROM video_playlist WHERE video_id=:id;
                                                DELETE FROM comments WHERE video_id=:id;
                                                DELETE FROM history WHERE video_id=:id;
                                                DELETE FROM video_tags WHERE video_id=:id;
                                                DELETE FROM likedetails WHERE videoid=:id;
                                                DELETE FROM videos WHERE id=:id");
            $stmt->bindParam(':id', $id, PDO::PARAM_STR);

            return $stmt->execute();

        } catch (PDOException $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * @param $id
     * @param $playlist_id
     * @return bool
     * @throws Exception
     */
    public function deleteFromPlaylist($id, $playlist_id)
    {
        try {
            $stmt = $this->conn->prepare("DELETE FROM video_playlist WHERE video_id=:id AND playlist_id=:playlist_id");

            $stmt->bindParam(':id', $id, PDO::PARAM_STR);
            $stmt->bindParam(':playlist_id', $playlist_id, PDO::PARAM_STR);

            return $stmt->execute();
        } catch (PDOException $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * @param $playlist_id
     * @return bool
     * @throws Exception
     */
    public function deletePlaylist($playlist_id)
    {
        try {
            $stmt = $this->conn->prepare("DELETE FROM video_playlist WHERE playlist_id=:playlist_id;
                                                    DELETE FROM playlists WHERE id = :playlist_id");
            $stmt->bindParam(':playlist_id', $playlist_id, PDO::PARAM_STR);

            return $stmt->execute();
        } catch (PDOException $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * @param $playlist_id
     * @return array
     * @throws Exception
     */
    public function getPlaylistContent($playlist_id)
    {
        try {
            $stmt = $this->conn->prepare("SELECT * FROM video_playlist INNER JOIN videos v ON video_playlist.video_id = v.id  WHERE playlist_id='$playlist_id'");

            $stmt->execute();
            $stmt->setFetchMode(PDO::FETCH_ASSOC);

            return $stmt->fetchAll();

        } catch (PDOException $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * @param $tag
     * @param $limit
     * @return array
     * @throws Exception
     */
    public function getByTag($tag, $limit)
    {
        try {
            $stmt = $this->conn->prepare("SELECT DISTINCT videos.video_url, videos.thumbnail, videos.title, 
                                            videos.description, videos.views, videos.likes, videos.created_at, videos.id 
                                            FROM videos
                                            INNER JOIN video_tags vt on videos.id = vt.video_id
                                            INNER JOIN tags t on vt.tag_id = t.id
                                            WHERE t.tag = :tag
                                            LIMIT :limit");
            $stmt->bindParam(':tag', $tag, PDO::PARAM_STR);
            $stmt->bindParam(':limit', $limit, PDO::PARAM_INT);

            $stmt->execute();
            $stmt->setFetchMode(PDO::FETCH_ASSOC);

            return $stmt->fetchAll();
        } catch (PDOException $e) {
            throw new Exception("Error: " . $e->getMessage());
        }
    }

    /**
     * @param Video $video
     * @param $video_id
     *
     * @return bool
     * @throws Exception
     */
    public function editVideo(Video $video, $video_id)
    {
        try {
            $stmt = $this->conn->prepare("UPDATE videos SET title = :title, description = :description WHERE id = :id");

            $stmt->bindParam(':title', $video->getTitle(), PDO::PARAM_STR);
            $stmt->bindParam(':description', $video->getDescription(), PDO::PARAM_STR);
            $stmt->bindParam(':id', $video_id, PDO::PARAM_INT);

            return $stmt->execute();
        } catch (PDOException $e) {
            throw new Exception("error: " . $e->getMessage());
        }
    }
}