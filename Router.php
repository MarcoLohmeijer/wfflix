<?php

class Router
{
    /** @var array */
    public $routes = [
        'GET' => [],
        'POST' => [],
    ];

    /**
     * @param $uri
     * @param $controller
     */
    public function get($uri, $controller)
    {
        $this->routes['GET'][$uri] = $controller;
    }

    /**
     * @param $uri
     * @param $controller
     */
    public function post($uri, $controller)
    {
        $this->routes['POST'][$uri] = $controller;
    }

    /**
     * @param $uri
     * @param $requestType
     *
     * @return mixed
     * @throws Exception
     */
    public function direct($uri, $requestType)
    {
        if (array_key_exists($uri, $this->routes[$requestType])) {
            if ($requestType === 'GET') {
                return $this->callControllerAction($this->routes[$requestType][$uri]);
            }

            if ($requestType === 'POST') {
                return $this->callAction($this->routes[$requestType][$uri]);
            }
        }

        throw new Exception('No route found!');
    }

    /**
     * @param $file
     *
     * @return static
     */
    public static function load($file)
    {
        $router = new static;
        require $file;

        return $router;
    }

    /**
     * @param $controller
     *
     * @return mixed
     */
    private function callControllerAction($controller)
    {
        $request = explode('@', $controller);
        $class = "src\\controllers\\$request[0]";

        return call_user_func(array(new $class, $request[1]));
    }

    /**
     * @param $action
     *
     * @return mixed
     */
    private function callAction($action)
    {
        $request = explode('@', $action);

        $class = "src\\actions\\$request[0]";
        return call_user_func(array(new $class, $request[1]));
    }
}