<?php

use src\actions\AdminRegisterUser;
use src\controllers\HistoryController;

$router->get('', 'HomeController@getHome');
$router->get('home', 'HomeController@getHome');

$router->get('register', 'UserController@getRegister');
$router->get('register-teacher', 'UserController@getRegisterTeacher');
$router->get('register-student', 'UserController@getRegisterStudent');
$router->post('action/create_user', 'RegisterUserAction@register');

$router->get('login', 'UserController@getLogin');
$router->post('login-action', 'LoginUserAction@login');
$router->get('logout', 'UserController@logout');

$router->post('actions/like-video', 'LikeVideoAction@like');
$router->post('comment-video', 'CommentVideoAction@insertAction');

$router->get('update-user', 'UserController@getUpdateUser');
$router->post('update-user-action', 'UpdateUserAction@update');

$router->get('change-password', 'UserController@getUpdatePassword');
$router->post('update-password-action', 'UpdatePasswordAction@updatePassword');

$router->get('user-history', 'HistoryController@getHistory');
$router->post('delete-history-action', 'DeleteHistoryAction@delete');

$router->get('user-comments', 'CommentController@getComments');
$router->post('delete-comment', 'DeleteCommentAction@delete');

//f
//$router->get('delete-user', 'src/views/deleteUser.php');
//$router->post('delete-user-action', '');
//
$router->get('upload', 'VideoController@getUpload');
$router->post('actions/upload-video', 'UploadVideoAction@upload');

$router->get('edit-video', 'VideoController@getUpdate');
$router->post('edit-video-action', 'EditVideoAction@edit');

$router->post('delete-video-action', 'DeleteVideoAction@delete');

$router->get('watch', 'VideoController@getWatch');
$router->get('watch-video-playlist','PlaylistController@getWatchPlaylist');

$router->get('my-playlists','UserController@getMyPlaylists');
$router->get('playlist-content','UserController@getPlaylistContent');
$router->get('my-uploads','UserController@getMyUploads');
$router->get('create-playlist','PlaylistController@getCreatePlaylist');
$router->post('create-playlist-action', 'CreatePlaylistAction@create');


$router->get('playlist-overview','PlaylistController@getPlaylistOverview');
$router->post('addToPlaylistAction','AddToPlaylistAction@add');
$router->post('delete-from-playlist-action','DeleteFromPlaylistAction@delete');
$router->post('delete-playlist','DeletePlaylistAction@delete');

/** admin routes */

$router->get('admin-overview','AdminController@getAdminOverview');
$router->get('admin-add-user','AdminController@getAdminAddUser');
$router->get('admin-show-users','AdminController@getAdminShowUsers');
$router->get('admin-show-videos','AdminController@getAdminShowVideos');

$router->post('admin-register-action', 'AdminRegisterUser@register');
$router->post('actions/admin-delete-user','AdminDeleteUser@adminDelete');
$router->post('actions/admin-delete-video', 'AdminDeleteVideo@adminDelete');
